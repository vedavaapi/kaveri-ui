# svelte issues to track

> Reactivity update twice when bind array or object on a component. #4265
    <https://github.com/sveltejs/svelte/issues/4265>
    <https://github.com/sveltejs/svelte/issues/4430>
    downgraded to 3.16.4. should upgrade after fix

> incorrect reactivity
    <https://github.com/sveltejs/svelte/issues/4933>
    <https://github.com/sveltejs/svelte/issues/4448>
    <https://github.com/sveltejs/svelte/issues/5156>
    see: SchemaForm
