/**
 * Synchronous loading of messages.
 * Should not be used in production code, as it will pull all locales
 * use only in storybook initialization, etc for immediate loading.
 */
// @ts-ignore
import { addMessages } from 'svelte-i18n';

// @ts-ignore
import en from '../messages/en.json';
// @ts-ignore
import te from '../messages/te.json';
// @ts-ignore
import sa from '../messages/sa.json';

addMessages('en', en);
addMessages('te', te);
addMessages('sa', sa);
