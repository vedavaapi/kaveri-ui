// eslint-disable-next-line no-unused-vars
import { MetaStore } from '@vedavaapi/classdefs/dist/stores/meta-store';
// eslint-disable-next-line no-unused-vars
import { ClassHierarchyStore } from '@vedavaapi/classdefs/dist/stores/class-hierarchy-store';
// eslint-disable-next-line no-unused-vars
import { UiProfileStore } from '@vedavaapi/classdefs/dist/stores/ui-profile-store';
// eslint-disable-next-line no-unused-vars
import { Context } from '@vedavaapi/client';
// eslint-disable-next-line no-unused-vars
import { writable, Writable, get } from 'svelte/store';

import { CLASS_DEFS_DB_NAME } from './names';

import { metaStoreRedisClient, chStoreRedisClient, uiProfileStoreRedisClient } from './classdefs-redis-clients'; // NOTE: isomorphic, doesn't run on client.

/**
 * @type { Writable<MetaStore> }
 */
export const metaStoreSq = writable(undefined); // MetaStoreStore = MetaStoreSq : )

/**
 * @type { Writable<ClassHierarchyStore> }
 */
export const classHierarchyStoreSq = writable(undefined);

/**
 * @type { Writable<UiProfileStore> }
 */
export const uiProfileStoreSq = writable(undefined);

/**
 * @param {Context} vc
 * @param {number} level
 */
export async function initClassdefsStores(vc, level, forceRefresh = false) {
    let metaStore = forceRefresh ? undefined : get(metaStoreSq);
    if (!metaStore) {
        metaStore = await MetaStore.init({ dbName: CLASS_DEFS_DB_NAME, redisClient: metaStoreRedisClient });
        metaStoreSq.set(metaStore);
    }
    if (level < 1) return;

    let chStore = forceRefresh ? undefined : get(classHierarchyStoreSq);
    if (!chStore) {
        chStore = await ClassHierarchyStore.init({ dbName: CLASS_DEFS_DB_NAME, metaStore, vc, redisClient: chStoreRedisClient });
        classHierarchyStoreSq.set(chStore);
    }
    if (level < 2) return;

    let upStore = forceRefresh ? undefined : get(uiProfileStoreSq);
    if (!upStore) {
        upStore = await UiProfileStore.init({ dbName: CLASS_DEFS_DB_NAME, metaStore, chStore, vc, redisClient: uiProfileStoreRedisClient, profilesUpdateCheckInterval: 1 * 3600 * 1000 });
        uiProfileStoreSq.set(upStore);
    }
}
