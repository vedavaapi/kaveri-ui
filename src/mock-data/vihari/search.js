import { LocaleContext } from '@vedavaapi/jsonld-helpers/dist/locale';
import { scannedBookProfileCompiled, libraryProfileCompiled, book1Repr, library1Repr } from '.';


export const mockLC = new LocaleContext({ locale: 'te', fallbackLocales: ['te', 'en', '*'] });

export const sbsp = scannedBookProfileCompiled.search;

export const lsp = libraryProfileCompiled.search;

export const sbssfs = sbsp.simple;
export const sbasfs = sbsp.advanced;
export const lssfs = lsp.simple;
export const lasfs = lsp.advanced;

export const sbafsMap = {};
sbasfs.forEach((fs) => {
    sbafsMap[fs.field] = fs;
});

export const mockSbFvMap = {
    title: book1Repr.name,
    source: library1Repr._id,
    description: 'ashtadhyayi',
    subject: 'vyakarana',
    row: 'A1B1C1',
};

export const mockSbFvMap2 = {
    jsonClass: 'ScannedBook',
};

export const mockSbFvMap3 = {
    jsonClass: 'ScannedBook',
    source: library1Repr._id,
};

export const mockSbFrMap = {
    title: 'is',
    source: 'is',
    description: 'has',
    subject: 'is',
    row: 'sw',
};

export const mockSbFrMap2 = {
    jsonClass: 'is',
};

export const mockSbFrMap3 = {
    jsonClass: 'is',
    source: 'is',
};

export const mockSbFvlMap = {
    source: library1Repr.name,
};

export const mockSbFvlMap3 = {
    source: library1Repr.name,
};
