import { compileAndSerializeUiProfileTemplates, deserializeUiProffileTemplates } from '@vedavaapi/classdefs/dist/utils';
import { UiCompiliedProfileTemplate } from '@vedavaapi/classdefs/dist/models/index';
// eslint-disable-next-line no-unused-vars
import { IUiProfile } from '@vedavaapi/classdefs/dist/types';
import { LocaleContext } from '@vedavaapi/jsonld-helpers/dist/locale';
// eslint-disable-next-line no-unused-vars
import { IResource } from '@vedavaapi/types/dist/Resource';

import { ResourceReprAccessor } from '../../routes/vihari/_components/res/accessors';
import { mockVC } from '../acls';

/**
 * @type IResource;
 */
export const mockLibrary = {
    _id: '5d5efa5ca05801000a8089b3',
    contributor: [
        '5d5edef5a05801000a8089ac',
    ],
    created: '2019-08-22 20:26:04.522413',
    creator: '5d5edef5a05801000a8089ac',
    jsonClass: 'Library',
    name: 'Guest Library',
    description: 'A guest library',
    type: 'oa:SpecificResource',
};

/**
 * @type IResource;
 */
export const book1 = {
    _id: '5d7df8c16864390012210d5e',
    author: [
        'Sri Kamalakar Bhatta',
    ],
    cover: {
        data: '_OOLD:5d7df8c068643900122109b1',
        jsonClass: 'StillImageRepresentation',
        type: 'owl:Thing',
    },
    created: '2019-09-15 08:39:28.824799',
    creator: '5d5edef5a05801000a8089ac',
    jsonClass: 'ScannedBook',
    metadata: [
        {
            jsonClass: 'MetadataItem',
            label: 'title',
            type: 'vv:MetadataItem',
            value: 'The Nirnaya Sindhu',
        },
        {
            jsonClass: 'MetadataItem',
            label: 'subject',
            type: 'vv:MetadataItem',
            value: 'City',
        },
        {
            jsonClass: 'MetadataItem',
            label: 'date',
            type: 'vv:MetadataItem',
            value: '1940',
        },
        {
            jsonClass: 'MetadataItem',
            label: 'creator',
            type: 'vv:MetadataItem',
            value: 'Sri Kamalakar Bhatta',
        },
        {
            jsonClass: 'MetadataItem',
            label: 'related',
            type: 'vv:MetadataItem',
            value: 'https://archive.org/download/in.ernet.dli.2015.326603/in.ernet.dli.2015.326603.pdf',
        },
        {
            jsonClass: 'MetadataItem',
            label: 'related',
            type: 'vv:MetadataItem',
            value: 'https://archive.org/stream/in.ernet.dli.2015.326603',
        },
    ],
    representations: {
        default: 'stillImage',
        interactiveResource: [
            {
                data: '_OOLD:5d7df8c068643900122109b3',
                interaction_type: 'archive_book_reader',
                jsonClass: 'DataRepresentation',
                type: 'owl:Thing',
            },
        ],
        jsonClass: 'DataRepresentations',
        stillImage: [
            {
                data: '_OOLD:5d7df8c068643900122109b2',
                jsonClass: 'StillImageRepresentation',
                mimetype: 'application/pdf',
                type: 'owl:Thing',
            },
        ],
        type: 'owl:Thing',
    },
    source: '5d5efa5ca05801000a8089b3',
    title: 'The Nirnaya Sindhu',
    type: 'dcterms:BibliographicResource',
};

/**
 * @type IResource;
 */
export const book2 = {
    _id: '5dc1466d549591000e5ff4df',
    author: [
        'eGangotri',
    ],
    cover: {
        data: '_OOLD:5dc1466d549591000e5ff4a4',
        jsonClass: 'StillImageRepresentation',
        type: 'owl:Thing',
    },
    created: '2019-11-05 09:52:45.736905',
    creator: '5d5edef5a05801000a8089ac',
    jsonClass: 'ScannedBook',
    metadata: [
        {
            jsonClass: 'MetadataItem',
            label: 'title',
            type: 'vv:MetadataItem',
            value: 'Rudryamal Tantra Bhavani Puja Paddhati 4866 Alm 22 Shlf 1 Devanagari Tantra',
        },
        {
            jsonClass: 'MetadataItem',
            label: 'subject',
            type: 'vv:MetadataItem',
            value: 'Manuscripts. Sanskrit. संस्कृत. पाण्डुलिपि',
        },
        {
            jsonClass: 'MetadataItem',
            label: 'creator',
            type: 'vv:MetadataItem',
            value: 'eGangotri',
        },
        {
            jsonClass: 'MetadataItem',
            label: 'related',
            type: 'vv:MetadataItem',
            value: 'https://archive.org/download/RudryamalTantraBhavaniPujaPaddhati4866Alm22Shlf1DevanagariTantra/RudryamalTantraBhavaniPujaPaddhati4866Alm22Shlf1DevanagariTantra.pdf',
        },
        {
            jsonClass: 'MetadataItem',
            label: 'related',
            type: 'vv:MetadataItem',
            value: 'https://archive.org/stream/RudryamalTantraBhavaniPujaPaddhati4866Alm22Shlf1DevanagariTantra',
        },
    ],
    representations: {
        default: 'stillImage',
        interactiveResource: [
            {
                data: '_OOLD:5dc1466d549591000e5ff4a6',
                interaction_type: 'archive_book_reader',
                jsonClass: 'DataRepresentation',
                type: 'owl:Thing',
            },
        ],
        jsonClass: 'DataRepresentations',
        stillImage: [
            {
                data: '_OOLD:5dc1466d549591000e5ff4a5',
                jsonClass: 'StillImageRepresentation',
                mimetype: 'application/pdf',
                type: 'owl:Thing',
            },
        ],
        type: 'owl:Thing',
    },
    source: '5d5efa5ca05801000a8089b3',
    title: 'Rudryamal Tantra Bhavani Puja Paddhati 4866 Alm 22 Shlf 1 Devanagari Tantra',
    type: 'dcterms:BibliographicResource',
};


/**
 * @type { IUiProfile }
 */
export const libraryProfile = {
    actions: [
        {
            id: 'libraries:edit',
            params: {
                library_id: '_id',
            },
        },
        {
            id: 'books:new',
            params: {
                library_id: '_id',
            },
        },
        {
            id: 'books:import:iiif',
            params: {
                library_id: '_id',
            },
        },
        {
            id: 'books:import:pdf',
            params: {
                library_id: '_id',
            },
        },
        {
            id: 'textdocs:new',
            params: {
                library_id: '_id',
            },
        },
    ],
    jsonClass: 'UiProfile',
    nav: {
        source: [
            {
                default_expanded: true,
                json_class: 'ScannedBook',
            },
            {
                json_class: 'TextDocument',
            },
            {
                json_class: 'Library',
                label: [
                    {
                        chars: 'Sub libraries',
                        jsonClass: 'Text',
                        language: 'en',
                    },
                    {
                        chars: 'ఉప గ్రంథాలయాలు',
                        jsonClass: 'Text',
                        language: 'te',
                    },
                ],
            },
        ],
    },
    repr: {
        cls: {
            label: [
                {
                    chars: 'Library',
                    jsonClass: 'Text',
                    language: 'en',
                },
                {
                    chars: 'గ్రంథాలయము',
                    jsonClass: 'Text',
                    language: 'te',
                },
            ],
            label_plural: [
                {
                    chars: 'Libraries',
                    jsonClass: 'Text',
                    language: 'en',
                },
                {
                    chars: 'గ్రంథాలయాలు',
                    jsonClass: 'Text',
                    language: 'te',
                },
            ],
        },
        instance: {
            description: 'description',
            img_repr: 'logo',
            name: 'name',
        },
    },
    search: {
        advanced: [
            {
                field: 'name',
                label: [
                    {
                        chars: 'Name',
                        jsonClass: 'Text',
                        language: 'en',
                    },
                    {
                        chars: 'పేరు',
                        jsonClass: 'Text',
                        language: 'te',
                    },
                ],
            },
            {
                field: 'source',
                type: '_id',
                allowedClasses: ['Library'],
                label: [
                    {
                        chars: 'Parent Library',
                        jsonClass: 'Text',
                        language: 'en',
                    },
                    {
                        chars: 'మాతృ గ్రంథాలయం',
                        jsonClass: 'Text',
                        language: 'te',
                    },
                ],
            },
            {
                field: 'description',
                label: [
                    {
                        chars: 'Description',
                        jsonClass: 'Text',
                        language: 'en',
                    },
                    {
                        chars: 'వివరణ',
                        jsonClass: 'Text',
                        language: 'te',
                    },
                ],
                type: 'number',
                required: true,
            },
        ],
        simple: {
            fields: [
                'name',
                'description',
            ],
            label: [
                {
                    chars: 'name or description',
                    jsonClass: 'Text',
                    language: 'en',
                },
                {
                    chars: 'పేరు లేదా వివరణ',
                    jsonClass: 'Text',
                    language: 'te',
                },
            ],
        },
    },
};

/**
 * @type { IUiProfile }
 */
export const scannedBookProfile = {
    actions: [
        {
            id: 'books:edit',
            params: {
                book_id: '_id',
            },
        },
    ],
    jsonClass: 'UiProfile',
    nav: {
        source: [
            {
                default_expanded: true,
                json_class: 'ScannedPage',
            },
        ],
        target: [
            {
                json_class: 'SequenceAnnotation',
            },
        ],
    },
    repr: {
        cls: {
            label: [
                {
                    chars: 'Book',
                    jsonClass: 'Text',
                    language: 'en',
                },
                {
                    chars: 'పుస్తకము',
                    jsonClass: 'Text',
                    language: 'te',
                },
            ],
            label_plural: [
                {
                    chars: 'Books',
                    jsonClass: 'Text',
                    language: 'en',
                },
                {
                    chars: 'పుస్తకాలు',
                    jsonClass: 'Text',
                    language: 'te',
                },
            ],
        },
        instance: {
            by: 'author',
            description: 'description',
            img_repr: 'cover',
            name: 'title',
        },
        projection: {
            representations: 0,
        },
    },
    search: {
        advanced: [
            {
                field: 'title',
                label: [
                    {
                        chars: 'Title',
                        jsonClass: 'Text',
                        language: 'en',
                    },
                    {
                        chars: 'పేరు',
                        jsonClass: 'Text',
                        language: 'te',
                    },
                ],
            },
            {
                field: 'author',
                label: [
                    {
                        chars: 'Author',
                        jsonClass: 'Text',
                        language: 'en',
                    },
                    {
                        chars: 'గ్రంథకర్త',
                        jsonClass: 'Text',
                        language: 'te',
                    },
                ],
                type: 'number', // TODO fix back
                required: true,
            },
            {
                field: 'source',
                allowedClasses: ['Library'],
                label: [
                    {
                        chars: 'Library',
                        jsonClass: 'Text',
                        language: 'en',
                    },
                    {
                        chars: 'గ్రంథాలయము',
                        jsonClass: 'Text',
                        language: 'te',
                    },
                ],
                type: '_id',
            },
        ],
        simple: {
            fields: [
                'title',
                'author',
            ],
            label: [
                {
                    chars: 'title or author',
                    jsonClass: 'Text',
                    language: 'en',
                },
                {
                    chars: 'పేరు లేదా కర్త',
                    jsonClass: 'Text',
                    language: 'te',
                },
            ],
        },
    },
};


export const mockLC = new LocaleContext({ locale: 'te', fallbackLocales: ['te', 'en', '*'] });

export const libraryProfileCompiled = new UiCompiliedProfileTemplate({
    uiProfile: deserializeUiProffileTemplates(compileAndSerializeUiProfileTemplates(libraryProfile)),
});

export const libraryAccessor = new ResourceReprAccessor({ clsProfile: libraryProfileCompiled.repr.cls, vc: mockVC, lc: mockLC });

export const scannedBookProfileCompiled = new UiCompiliedProfileTemplate({
    uiProfile: deserializeUiProffileTemplates(compileAndSerializeUiProfileTemplates(scannedBookProfile)),
});

export const bookAccessor = new ResourceReprAccessor({ clsProfile: scannedBookProfileCompiled.repr.cls, vc: mockVC, lc: mockLC });


export const library1Repr = libraryProfileCompiled.repr.instance({ _: mockLibrary });

export const book1Repr = scannedBookProfileCompiled.repr.instance({ _: book1, p: mockLibrary, pr: library1Repr });

export const book2Repr = scannedBookProfileCompiled.repr.instance({ _: book2, p: mockLibrary, pr: library1Repr });


export const allClassNames = ['JsonObject', 'Resource', 'Annotation', 'FragmentSelector', 'Selector', 'ScannedBook', 'Library', 'ChoiceAnnotation'];
