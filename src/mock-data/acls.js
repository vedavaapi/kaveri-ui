// eslint-disable-next-line no-unused-vars
import { IUser } from '@vedavaapi/client/dist/api/accounts/resource-models';
// eslint-disable-next-line no-unused-vars
import { IGraph } from '@vedavaapi/client/dist/api/objstore/response-models';
import { resolveAcls, getInResourceHierarchy, getDefaultResHierarchyBranch } from '@vedavaapi/acls/dist/resolver';
// eslint-disable-next-line no-unused-vars
import { IAclsGraph } from '@vedavaapi/acls/dist/acls';


export const mockVC = {
    base: 'https://repo1.ebharati.org/api',
    accessToken: '6k0OyyxfUr83zzHf0zt8MUjeIGNPpAJObOVvNKIcXP',
};


export const ownPresense = {
    hasInOwn: true,
};

export const inheritedPresense = {
    inheritedFrom: '5c3f5acd365',
};

export const bothPresense = {
    hasInOwn: true,
    inheritedFrom: '5c3f5acd365',
};


export const mockUsers = {
    u1:
    {
        _id: '5e52975d3bc959000ae2ffa9',
        created: '2020-02-23 15:16:45.462966',
        email: 'info@mock.org',
        jsonClass: 'User',
        name: 'Info',
        type: 'foaf:Agent',
    },
    u2:
    {
        _id: '5e52975d3bc959000ae2ffa7',
        created: '2020-02-23 15:16:45.465432',
        email: 'damodara@mock.org',
        jsonClass: 'User',
        name: 'damodara',
        picture: 'https://lh3.googleusercontent.com/a-/AAuE7mCD-2GYyDeAAezRypER6WjpZqIi83lLFFYBYMUYOg',
        type: 'foaf:Agent',
    },
    u3:
    {
        _id: '5e52975d3bc959000ae2ffa8',
        created: '2020-02-23 15:16:45.470294',
        email: 'aparna@mock.org',
        jsonClass: 'User',
        name: 'aparna',
        picture: 'https://4.bp.blogspot.com/-HivIQdXi-yo/U4hKVF_5nnI/AAAAAAAAAqA/oYzHawlk_4o/s113/*',
        type: 'foaf:Agent',
    },
    u4:
    {
        _id: '5e52975d3bc959000ae2ffaa',
        created: '2020-02-23 15:16:45.478649',
        email: 'alex@mock.org',
        jsonClass: 'User',
        name: 'alex',
        type: 'foaf:Agent',
    },
    u5:
    {
        _id: '5e52975d3bc959000ae2ffab',
        created: '2020-02-23 15:16:45.477592',
        email: 'aravindan@mock.org',
        jsonClass: 'User',
        name: 'aravindan',
        picture: 'https://pbs.twimg.com/profile_images/1104028147039653890/R1u76P6d_400x400.png',
        type: 'foaf:Agent',
    },
};


export const mockUsersGraph = {};

Object.keys(mockUsers).forEach((ind) => {
    const user = mockUsers[ind];
    mockUsersGraph[user._id] = user;
});


export const mockTeams = {
    t1:
    {
        _id: '5f11a993f483db0007620358',
        contributor: ['5e52975d3bc959000ae2ffa9'],
        created: '2020-02-23 15:16:45.687359',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Team',
        members: ['5e52975d3bc959000ae2ffa9'],
        name: 'Team 1',
        source: 'ALL_USERS_TEAM',
        type: 'foaf:Agent',
    },
    t2:
    {
        _id: '5e52975d3bc959000ae2ffac',
        contributor: ['5e52975d3bc959000ae2ffa9'],
        created: '2020-02-23 15:16:45.670421',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Team',
        members: ['5e52975d3bc959000ae2ffa9'],
        name: 'Team 2',
        source: 'ALL_USERS_TEAM',
        type: 'foaf:Agent',
    },
    t3:
    {
        _id: '5e52975d3bc959000ae2ffb0',
        contributor: ['5e52975d3bc959000ae2ffa9'],
        created: '2020-02-23 15:16:45.699693',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Team',
        members: ['5e52975d3bc959000ae2ffa9'],
        name: 'Team 3',
        source: 'ALL_USERS_TEAM',
        type: 'foaf:Agent',
    },
    t4:
    {
        _id: '5e52975d3bc959000ae2ffaf',
        contributor: ['5e52975d3bc959000ae2ffa9'],
        created: '2020-02-23 15:16:45.688425',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Team',
        members: ['5e52975d3bc959000ae2ffa9'],
        name: 'Team 4',
        source: 'ALL_USERS_TEAM',
        type: 'foaf:Agent',
    },
    t5:
    {
        _id: '5e52975d3bc959000ae2ffad',
        contributor: ['5e52975d3bc959000ae2ffa9'],
        created: '2020-02-23 15:16:45.671971',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Team',
        members: ['5e52975d3bc959000ae2ffa9'],
        name: 'Team 5',
        source: 'ALL_USERS_TEAM',
        type: 'foaf:Agent',
    },
};

export const guestUsersTeam = {
    _id: '5d5efa6fa05801000a8089b4',
    name: 'GuestUsers',
    jsonClass: 'Team',
    source: 'ALL_USERS_TEAM',
};

export const mockTeamsGraph = {};

Object.keys(mockTeams).forEach((ind) => {
    const team = mockTeams[ind];
    mockTeamsGraph[team._id] = team;
});


export const mockAgentsGraph = {};
Object.assign(mockAgentsGraph, mockUsersGraph);
Object.assign(mockAgentsGraph, mockTeamsGraph);


export const mockResources = {
    r1:
    {
        _id: '5e52975d3bc959000ae2ffb1',
        created: '2020-02-23 15:16:45.819839',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Library',
        jsonClassLabel: 'MockResource',
        name: 'R-1',
        // source: '5e5254273bc959000ae2ff7a',
        type: 'oa:SpecificResource',
        resolvedPermissions: {
            updatePermissions: true,
        },
    },
    r2:
    {
        _id: '5e52975d3bc959000ae2ffb2',
        created: '2020-02-23 15:16:45.819852',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Resource',
        name: 'R - 2',
        source: '5e52975d3bc959000ae2ffb1',
        type: 'oa:SpecificResource',
        resolvedPermissions: {
            updatePermissions: false,
        },
    },
    r3:
    {
        _id: '5e52975d3bc959000ae2ffb3',
        created: '2020-02-23 15:16:45.819857',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Resource',
        name: 'R - 3',
        source: '5e52975d3bc959000ae2ffb2',
        type: 'oa:SpecificResource',
        resolvedPermissions: {
            updatePermissions: true,
        },
    },
    r4:
    {
        _id: '5e52975d3bc959000ae2ffb4',
        created: '2020-02-23 15:16:45.819860',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Resource',
        name: 'R - 4',
        source: '5e52975d3bc959000ae2ffb3',
        type: 'oa:SpecificResource',
        resolvedPermissions: {
            updatePermissions: true,
        },
    },
    r5:
    {
        _id: '5e52975d3bc959000ae2ffb5',
        created: '2020-02-23 15:16:45.819863',
        creator: '5e52975d3bc959000ae2ffa9',
        jsonClass: 'Resource',
        name: 'R - 5',
        source: '5e52975d3bc959000ae2ffb4',
        type: 'oa:SpecificResource',
        resolvedPermissions: {
            updatePermissions: true,
        },
    },
};


/**
 * @type {IGraph}
 */
export const mockResGraph = {};

Object.keys(mockResources).forEach((ind) => {
    const res = mockResources[ind];
    mockResGraph[res._id] = res;
});

/**
 * @type { IAclsGraph }
 */
export const mockAclsGraph = { '5e5254273bc959000ae2ff7a': { _id: '5e5254273bc959000ae2ff7a', createAnnos: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, createChildren: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, delete: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, jsonClass: 'Acl', read: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['ALL_USERS_TEAM'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, type: 'vv:Acl', updateContent: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updateLinks: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updatePermissions: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' } }, '5e52975d3bc959000ae2ffb1': { _id: '5e52975d3bc959000ae2ffb1', createAnnos: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffae'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9', '5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, createChildren: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffae'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9', '5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, delete: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffae'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9', '5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, jsonClass: 'Acl', read: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffae'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9', '5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, type: 'vv:Acl', updateContent: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffae'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9', '5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updateLinks: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffae'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9', '5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updatePermissions: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa9'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' } }, '5e52975d3bc959000ae2ffb2': { _id: '5e52975d3bc959000ae2ffb2', createAnnos: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac', '5e52975d3bc959000ae2ffb0', '5e52975d3bc959000ae2ffaf'], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, createChildren: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac', '5e52975d3bc959000ae2ffb0', '5e52975d3bc959000ae2ffaf'], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, delete: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac', '5e52975d3bc959000ae2ffb0', '5e52975d3bc959000ae2ffaf'], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, jsonClass: 'Acl', read: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac', '5e52975d3bc959000ae2ffb0', '5e52975d3bc959000ae2ffaf'], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, type: 'vv:Acl', updateContent: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac', '5e52975d3bc959000ae2ffb0', '5e52975d3bc959000ae2ffaf'], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updateLinks: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac', '5e52975d3bc959000ae2ffb0', '5e52975d3bc959000ae2ffaf'], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updatePermissions: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' } }, '5e52975d3bc959000ae2ffb3': { _id: '5e52975d3bc959000ae2ffb3', createAnnos: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7', '5e52975d3bc959000ae2ffa8'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, createChildren: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7', '5e52975d3bc959000ae2ffa8'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, delete: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7', '5e52975d3bc959000ae2ffa8'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, jsonClass: 'Acl', read: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7', '5e52975d3bc959000ae2ffa8'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, type: 'vv:Acl', updateContent: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7', '5e52975d3bc959000ae2ffa8'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updateLinks: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7', '5e52975d3bc959000ae2ffa8'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updatePermissions: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' } }, '5e52975d3bc959000ae2ffb4': { _id: '5e52975d3bc959000ae2ffb4', createAnnos: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7'] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, createChildren: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7'] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, delete: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7'] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, jsonClass: 'Acl', read: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7'] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, type: 'vv:Acl', updateContent: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7'] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updateLinks: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffa7'] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updatePermissions: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' } }, '5e52975d3bc959000ae2ffb5': { _id: '5e52975d3bc959000ae2ffb5', createAnnos: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffaf', '5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffaa', '5e52975d3bc959000ae2ffab'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, createChildren: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffaf', '5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffaa', '5e52975d3bc959000ae2ffab'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, delete: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffaf', '5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffaa', '5e52975d3bc959000ae2ffab'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, jsonClass: 'Acl', read: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffaf', '5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffaa', '5e52975d3bc959000ae2ffab'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, type: 'vv:Acl', updateContent: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffaf', '5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffaa', '5e52975d3bc959000ae2ffab'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updateLinks: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffaf', '5e52975d3bc959000ae2ffad'], type: 'foaf:Agent', users: ['5e52975d3bc959000ae2ffaa', '5e52975d3bc959000ae2ffab'] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: ['5e52975d3bc959000ae2ffac'], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' }, updatePermissions: { block: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, grant: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, jsonClass: 'AccessControl', revoke: { jsonClass: 'AgentIds', teams: [], type: 'foaf:Agent', users: [] }, type: 'vv:ActionControl' } } };


export const mockRes = mockResources.r5;


export const mockResHierarchy = getInResourceHierarchy(mockResGraph, mockRes._id);

export const mockResHierarchyBranch = getDefaultResHierarchyBranch(mockResHierarchy);


export const mockResolvedAclsGraph = resolveAcls({
    aclsGraph: mockAclsGraph,
    resGraph: mockResGraph,
    resId: mockRes._id,
});


export const mockResolvedAcl = mockResolvedAclsGraph[mockRes._id];

export const mockActionResolvedAgents = mockResolvedAcl.delete;

export const mockInheritedAgentSet = mockActionResolvedAgents.granted;

export const mockUsersPresenseMap = mockInheritedAgentSet.users;

export const mockTeamsPresenseMap = mockInheritedAgentSet.teams;
