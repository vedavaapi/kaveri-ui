export const user = {
    _id: '5dc1466d549591000e5ff4df',
    jsonClass: 'User',
    name: 'damodara',
    email: 'damodara@example.com',
    picture: 'https://lh3.googleusercontent.com/a-/AAuE7mCD-2GYyDeAAezRypER6WjpZqIi83lLFFYBYMUYOg',
};

export const unnamedUser = {
    _id: '5dc1466d549591000e5ff4df',
    jsonClass: 'User',
    email: 'aname.12@example.com',
};

export const longnamedUser = {
    _id: '5dc1466d549591000e5ff4df',
    jsonClass: 'User',
    name: 'challa damodara reddy',
    email: 'damodara@example.com',
    picture: 'https://lh3.googleusercontent.com/a-/AAuE7mCD-2GYyDeAAezRypER6WjpZqIi83lLFFYBYMUYOg',
};


export const team = {
    _id: 'ALL_USERS_TEAM',
    jsonClass: 'Team',
    name: 'All Users',
};


export const userWithoutImage = {
    _id: '5dc1466d549591000e5ff4df',
    jsonClass: 'User',
    name: 'damodara',
    email: 'damodara@example.com',
};


export const unauthorizedSession = {};


export const authorizedSession = {
    accessToken: '6k0OyyxfUr83zzHf0zt8MUjeIGNPpAJObOVvNKIcXP',
    expiresIn: 500000,
    issuedAt: Date.now(),
    user,
    authorized: true,
};
