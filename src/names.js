export const SESSION_COOKIE_NAME = 'kaveri-session';
export const APPS_COOKIE_NAME = 'kaveri-authorized-apps';
export const CONF_COOKIE_NAME = 'kaveri-conf';

export const SESSION_SYNC_CHANNEL_NAME = 'session_sync_channel';
export const AUTH_WINDOW_NAME = 'AUTHORIZATION_POPUP';
export const UNAUTH_WINDOW_NAME = 'UNAUTHORIZATION_POPUP';


export const CLASS_DEFS_DB_NAME = 'kaveri-classdefs';
