import cors from 'cors';

export const allowWilCardOriginMiddleware = cors();

export const allowRequestedOriginMiddleware = cors({
    origin: (origin, callback) => {
        callback(null, true);
    },
});

export const allowRequestedOriginWithCredsMiddleware = cors({
    origin: (origin, callback) => {
        callback(null, true);
    },
    credentials: true,
});
