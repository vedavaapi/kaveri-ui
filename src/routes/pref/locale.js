import dset from 'dset';
import { SESSION_COOKIE_NAME } from '../../names';
import { urlencodedBodyParser } from '../../body-parser-middlewares';


export async function post(req, res) {
    urlencodedBodyParser(req, res, () => {
        const { lang = 'en' } = req.body;
        dset(req.ksession, 'session.conf.lang', lang);
        res.cookie(SESSION_COOKIE_NAME, req.ksession, { signed: true });
        res.status(200);
        res.json({
            status: 'SUCCESS',
            locale: lang,
        });
    });
}
