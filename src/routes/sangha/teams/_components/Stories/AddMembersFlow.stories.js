import '../../../../../storybook-init';
import { mockVC, mockTeams } from '../../../../../mock-data/acls';
import AddMembersFlow from '../AddMembersFlow.svelte';

export default {
    title: 'teams/AddMembersFlow',
};

export const ideal = () => ({
    Component: AddMembersFlow,
    props: {
        teamId: mockTeams.t1._id,
        vc: mockVC,
    },
});
