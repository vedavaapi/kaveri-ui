import '../../../../../storybook-init';
import { mockVC, mockTeams } from '../../../../../mock-data/acls';
import NewUserFlow from '../NewUserFlow.svelte';

export default {
    title: 'teams/NewUserFlow',
};

export const ideal = () => ({
    Component: NewUserFlow,
    props: {
        initialTeamId: mockTeams.t1._id,
        vc: mockVC,
    },
});
