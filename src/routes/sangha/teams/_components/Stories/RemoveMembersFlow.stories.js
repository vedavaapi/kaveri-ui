import '../../../../../storybook-init';
import { mockVC, mockTeams } from '../../../../../mock-data/acls';
import RemoveMembersFlow from '../RemoveMembersFlow.svelte';

export default {
    title: 'teams/RemoveMembersFlow',
};

export const ideal = () => ({
    Component: RemoveMembersFlow,
    props: {
        teamId: mockTeams.t1._id,
        vc: mockVC,
        members: [],
    },
});
