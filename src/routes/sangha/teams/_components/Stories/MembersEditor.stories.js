import '../../../../../storybook-init';
import { mockVC, guestUsersTeam } from '../../../../../mock-data/acls';
import MembersEditor from '../MembersEditor.svelte';

export default {
    title: 'teams/MembersEditor',
};

export const ideal = () => ({
    Component: MembersEditor,
    props: {
        team: guestUsersTeam,
        vc: mockVC,
    },
});
