export function getAgentType(agent) {
    return `${agent.jsonClass.toLowerCase()}s`;
}


export const ResHelper = {
    name(res) {
        return res.title || res.name || '';
    },

    classLabel(res) {
        return `${res.jsonClass}${res.jsonClassLabel ? `(${res.jsonClassLabel})` : ''}`;
    },

    label(res) {
        return `${this.name(res)}: ${this.classLabel(res)}`;
    },
};
