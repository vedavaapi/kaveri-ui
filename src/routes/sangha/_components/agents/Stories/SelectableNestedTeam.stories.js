
import '../../../../../storybook-init';
import { team } from '../../../../../mock-data/auth';
import { mockVC } from '../../../../../mock-data/acls';
import SelectableNestedTeam from '../SelectableNestedTeam.svelte';


export default {
    title: 'agents/SelectableNestedTeam',
};


export const ideal = () => ({
    Component: SelectableNestedTeam,
    props: {
        team,
        vc: mockVC,
    },
});
