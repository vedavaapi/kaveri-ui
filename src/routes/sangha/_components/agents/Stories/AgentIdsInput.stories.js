import { AclAgentType } from '@vedavaapi/acls/dist/agent-types';

import '../../../../../storybook-init';
import { mockVC } from '../../../../../mock-data/acls';
import AgentIdsInput from '../AgentIdsInput.svelte';

export default {
    title: 'agents/AgentIdsInput',
};

export const SingleTeam = () => ({
    Component: AgentIdsInput,
    props: {
        vc: mockVC,
        agentType: AclAgentType.TEAMS,
        agentId: 'ROOT_ADMINS_TEAM',
    },
});

export const MultipleTeams = () => ({
    Component: AgentIdsInput,
    props: {
        vc: mockVC,
        agentType: AclAgentType.TEAMS,
        agentIds: ['ROOT_ADMINS_TEAM'],
        mode: 'multi',
    },
});

export const SingleUser = () => ({
    Component: AgentIdsInput,
    props: {
        vc: mockVC,
        agentType: AclAgentType.USERS,
    },
});

export const MultipleUsers = () => ({
    Component: AgentIdsInput,
    props: {
        vc: mockVC,
        agentType: AclAgentType.USERS,
        mode: 'multi',
    },
});
