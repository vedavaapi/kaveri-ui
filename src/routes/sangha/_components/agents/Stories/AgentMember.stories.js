
import '../../../../../storybook-init';
import { user, team } from '../../../../../mock-data/auth';
import AgentMember from './AgentMember.view.svelte';


export default {
    title: 'agents/AgentMember',
};


export const User = () => ({
    Component: AgentMember,
    props: {
        agent: user,
    },
});

export const Team = () => ({
    Component: AgentMember,
    props: {
        agent: team,
    },
});
