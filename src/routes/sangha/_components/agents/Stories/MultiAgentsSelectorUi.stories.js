import { AclAgentType } from '@vedavaapi/acls/dist/agent-types';

import '../../../../../storybook-init';
import { mockVC } from '../../../../../mock-data/acls';
import MultiAgentsSelectorUi from '../MultiAgentsSelectorUi.svelte';


export default {
    title: 'agents/MultiAgentsSelectorUi',
};


export const ideal = () => ({
    Component: MultiAgentsSelectorUi,
    props: {
        vc: mockVC,
    },
});


export const usersOnly = () => ({
    Component: MultiAgentsSelectorUi,
    props: {
        vc: mockVC,
        allowedAgentTypes: [AclAgentType.USERS],
    },
});


export const teamsOnly = () => ({
    Component: MultiAgentsSelectorUi,
    props: {
        vc: mockVC,
        allowedAgentTypes: [AclAgentType.TEAMS],
    },
});
