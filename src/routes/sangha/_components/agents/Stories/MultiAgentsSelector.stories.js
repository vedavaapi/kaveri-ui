import { AclAgentType } from '@vedavaapi/acls/dist/agent-types';

import '../../../../../storybook-init';
import { mockVC } from '../../../../../mock-data/acls';
import MultiAgentsSelector from '../MultiAgentsSelector.svelte';


export default {
    title: 'agents/MultiAgentsSelector',
};


export const ideal = () => ({
    Component: MultiAgentsSelector,
    props: {
        vc: mockVC,
    },
});

export const usersOnly = () => ({
    Component: MultiAgentsSelector,
    props: {
        vc: mockVC,
        allowedAgentTypes: [AclAgentType.USERS],
    },
});

export const temasOnly = () => ({
    Component: MultiAgentsSelector,
    props: {
        vc: mockVC,
        allowedAgentTypes: [AclAgentType.TEAMS],
    },
});
