
import { AclAgentType } from '@vedavaapi/acls/dist/agent-types';

import '../../../../../storybook-init';
import { mockVC } from '../../../../../mock-data/acls';
import AgentsSelector from '../AgentsSelector.svelte';


export default {
    title: 'agents/AgentsSelector',
};


export const usersSelector = () => ({
    Component: AgentsSelector,
    props: {
        vc: mockVC,
        agentsType: AclAgentType.USERS,
    },
});


export const teamsSelector = () => ({
    Component: AgentsSelector,
    props: {
        vc: mockVC,
        agentsType: AclAgentType.TEAMS,
    },
});
