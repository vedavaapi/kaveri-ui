
import '../../../../../storybook-init';
import { mockUsersGraph, mockTeamsGraph, mockAgentsGraph } from '../../../../../mock-data/acls';
import AgentMembersSet from '../AgentMembersSet.svelte';


export default {
    title: 'agents/AgentMembersSet',
};


export const ideal = () => ({
    Component: AgentMembersSet,
    props: {
        agentsSet: {
            users: Object.keys(mockUsersGraph).slice(0, 3),
            teams: Object.keys(mockTeamsGraph).slice(0, 2),
        },
        agentsGraph: mockAgentsGraph,
    },
});
