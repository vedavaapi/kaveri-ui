
import '../../../../../storybook-init';
import { AclAgentType } from '@vedavaapi/acls/dist/agent-types';
import { mockUsers, mockTeams, mockAgentsGraph } from '../../../../../mock-data/acls';
import SelectableAgentsList from '../SelectableAgentsList.svelte';


export default {
    title: 'agents/SelectableAgentsList',
};


export const Users = () => ({
    Component: SelectableAgentsList,
    props: {
        agentsIds: Object.values(mockUsers).map((v) => v._id),
        agentsGraph: mockAgentsGraph,
        agentsType: AclAgentType.USERS,
    },
});


export const Teams = () => ({
    Component: SelectableAgentsList,
    props: {
        agentsIds: Object.values(mockTeams).map((v) => v._id),
        agentsGraph: mockAgentsGraph,
        agentsType: AclAgentType.TEAMS,
    },
});
