
import '../../../../../storybook-init';
import { mockUsersGraph } from '../../../../../mock-data/acls';
import AgentMembersList from './AgentMembersList.view.svelte';


export default {
    title: 'agents/AgentMembersList',
};


export const ideal = () => ({
    Component: AgentMembersList,
    props: {
        agentIds: Object.keys(mockUsersGraph),
        agentsGraph: mockUsersGraph,
    },
});
