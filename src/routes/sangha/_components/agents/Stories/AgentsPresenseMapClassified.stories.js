import { AclAgentType } from '@vedavaapi/acls/dist/agent-types';

import '../../../../../storybook-init';
import { mockUsersPresenseMap, mockTeamsPresenseMap, mockAgentsGraph } from '../../../../../mock-data/acls';

import AgentsPresenseMapClassified from '../AgentsPresenseMapClassified.svelte';


export default {
    title: 'agents/AgentsPresenseMapClassified',
};


export const usersPresenseMap = () => ({
    Component: AgentsPresenseMapClassified,
    props: {
        agentIdPresenseMap: mockUsersPresenseMap,
        agentsGraph: mockAgentsGraph,
        agentsType: AclAgentType.USERS,
    },
});


export const teamsPresenseMap = () => ({
    Component: AgentsPresenseMapClassified,
    props: {
        agentIdPresenseMap: mockTeamsPresenseMap,
        agentsGraph: mockAgentsGraph,
        agentsType: AclAgentType.TEAMS,
    },
});
