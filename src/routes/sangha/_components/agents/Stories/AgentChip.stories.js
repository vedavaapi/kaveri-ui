
import '../../../../../storybook-init';
import { user, team, unnamedUser, longnamedUser } from '../../../../../mock-data/auth';

import AgentChip from './AgentChip.view.svelte';


export default {
    title: 'agents/AgentChip',
};


export const User = () => ({
    Component: AgentChip,
    props: {
        agent: user,
    },
});


export const UnnamedUser = () => ({
    Component: AgentChip,
    props: {
        agent: unnamedUser,
    },
});


export const LongNamedUser = () => ({
    Component: AgentChip,
    props: {
        agent: longnamedUser,
    },
});


export const Team = () => ({
    Component: AgentChip,
    props: {
        agent: team,
    },
});
