
import '../../../../../storybook-init';
import { user, team } from '../../../../../mock-data/auth';
import SelectableAgent from '../SelectableAgent.svelte';


export default {
    title: 'agents/SelectableAgent',
};


export const User = () => ({
    Component: SelectableAgent,
    props: {
        agent: user,
    },
});


export const Team = () => ({
    Component: SelectableAgent,
    props: {
        agent: team,
    },
});
