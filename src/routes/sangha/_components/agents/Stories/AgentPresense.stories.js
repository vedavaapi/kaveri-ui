import '../../../../../storybook-init';
import { user, team } from '../../../../../mock-data/auth';
import { ownPresense, bothPresense } from '../../../../../mock-data/acls';

import AgentPresense from './AgentPresense.view.svelte';


export default {
    title: 'agents/AgentPresense',
};


const on = {
    'inherit-link-click': () => {
        // console.log('event:inherit-link-click');
    },
};


export const User = () => ({
    Component: AgentPresense,
    props: {
        agent: user,
        presense: ownPresense,
    },
    on,
});


export const InheritedUser = () => ({
    Component: AgentPresense,
    props: {
        agent: user,
        presense: bothPresense,
    },
    on,
});


export const Team = () => ({
    Component: AgentPresense,
    props: {
        agent: team,
        presense: ownPresense,
    },
    on,
});


export const InheritedTeam = () => ({
    Component: AgentPresense,
    props: {
        agent: team,
        presense: bothPresense,
    },
    on,
});
