import { AclAgentType } from '@vedavaapi/acls/dist/agent-types';

import '../../../../../storybook-init';
import { mockUsersPresenseMap, mockTeamsPresenseMap, mockAgentsGraph } from '../../../../../mock-data/acls';

import AgentsPresenseMap from '../AgentsPresenseMap.svelte';


export default {
    title: 'agents/AgentsPresenseMap',
};


export const usersPresenseMap = () => ({
    Component: AgentsPresenseMap,
    props: {
        agentIdPresenseMap: mockUsersPresenseMap,
        agentsGraph: mockAgentsGraph,
        agentsType: AclAgentType.USERS,
    },
});


export const teamsPresenseMap = () => ({
    Component: AgentsPresenseMap,
    props: {
        agentIdPresenseMap: mockTeamsPresenseMap,
        agentsGraph: mockAgentsGraph,
        agentsType: AclAgentType.TEAMS,
    },
});
