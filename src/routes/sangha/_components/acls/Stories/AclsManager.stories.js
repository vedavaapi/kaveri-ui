
import '../../../../../storybook-init';
import { mockVC, mockRes } from '../../../../../mock-data/acls';
import AclsManager from '../AclsManager.svelte';


export default {
    title: 'acls/AclsManager',
};


export const ideal = () => ({
    Component: AclsManager,
    props: {
        vc: mockVC,
        resId: mockRes._id,
    },
});
