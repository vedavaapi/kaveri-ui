
import '../../../../../storybook-init';
import { mockResolvedAclsGraph, mockResHierarchy, mockResGraph, mockAgentsGraph, mockVC } from '../../../../../mock-data/acls';

import ResolvedAclsHierararchy from '../ResolvedAclsHierararchy.svelte';


export default {
    title: 'acls/ResolvedAclsHierararchy',
};


export const ideal = () => ({
    Component: ResolvedAclsHierararchy,
    props: {
        resGraph: mockResGraph,
        agentsGraph: mockAgentsGraph,
        hierarchy: mockResHierarchy,
        resolvedAclsGraph: mockResolvedAclsGraph,
        vc: mockVC,
    },
});
