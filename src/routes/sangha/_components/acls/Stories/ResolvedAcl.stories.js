
import '../../../../../storybook-init';
import { mockResolvedAcl, mockAgentsGraph } from '../../../../../mock-data/acls';

import ResolvedAcl from '../ResolvedAcl.svelte';


export default {
    title: 'acls/ResolvedAcl',
};


export const ideal = () => ({
    Component: ResolvedAcl,
    props: {
        resolvedAcl: mockResolvedAcl,
        agentsGraph: mockAgentsGraph,
    },
});
