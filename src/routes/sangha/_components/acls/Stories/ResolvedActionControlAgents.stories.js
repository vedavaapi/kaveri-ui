import { AclControl } from '@vedavaapi/acls/dist/controls';

import '../../../../../storybook-init';
import { mockActionResolvedAgents, mockAgentsGraph } from '../../../../../mock-data/acls';

import ResolvedActionControlAgents from '../ResolvedActionControlAgents.svelte';


export default {
    title: 'acls/ResolvedActionControlAgents',
};


export const granted = () => ({
    Component: ResolvedActionControlAgents,
    props: {
        control: AclControl.GRANT,
        inheritedAgentSet: mockActionResolvedAgents.granted,
        agentsGraph: mockAgentsGraph,
        opsAvailablities: { add: true, remove: true, revokeInherited: true },
    },
});


export const blocked = () => ({
    Component: ResolvedActionControlAgents,
    props: {
        control: AclControl.GRANT,
        inheritedAgentSet: mockActionResolvedAgents.blocked,
        agentsGraph: mockAgentsGraph,
        opsAvailablities: { add: true, remove: true },
    },
});


export const revoked = () => ({
    Component: ResolvedActionControlAgents,
    props: {
        control: AclControl.GRANT,
        inheritedAgentSet: mockActionResolvedAgents.revoked,
        agentsGraph: mockAgentsGraph,
        opsAvailablities: { remove: true },
    },
});
