
import '../../../../../storybook-init';
import { mockResolvedAcl, mockAgentsGraph, mockRes } from '../../../../../mock-data/acls';
import ResolvedAclResWrapper from '../ResolvedAclResWrapper.svelte';


export default {
    title: 'acls/ResolvedAclResWrapper',
};


export const ideal = () => ({
    Component: ResolvedAclResWrapper,
    props: {
        resolvedAcl: mockResolvedAcl,
        agentsGraph: mockAgentsGraph,
        res: mockRes,
    },
});
