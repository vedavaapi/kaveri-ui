
import '../../../../../storybook-init';
import { mockResHierarchyBranch, mockResolvedAclsGraph, mockAgentsGraph, mockResGraph } from '../../../../../mock-data/acls';

import ResolvedAclsHierarchyBranch from '../ResolvedAclsHierarchyBranch.svelte';


export default {
    title: 'acls/ResolvedAclsHierarchyBranch',
};


export const ideal = () => ({
    Component: ResolvedAclsHierarchyBranch,
    props: {
        resolvedAclsGraph: mockResolvedAclsGraph,
        hierarchyBranch: mockResHierarchyBranch,
        resGraph: mockResGraph,
        agentsGraph: mockAgentsGraph,
    },
});
