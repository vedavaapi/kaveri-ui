
import '../../../../../storybook-init';
import { mockResHierarchy, mockResGraph } from '../../../../../mock-data/acls';
import ResHierarchyContext from '../ResHierarchyContext.svelte';


export default {
    title: 'acls/ResHierarchyContext',
};


export const ideal = () => ({
    Component: ResHierarchyContext,
    props: {
        hierarchy: mockResHierarchy,
        resGraph: mockResGraph,
    },
});

