import { AclAction } from '@vedavaapi/acls/dist/actions';
import '../../../../../storybook-init';
import { mockActionResolvedAgents, mockAgentsGraph } from '../../../../../mock-data/acls';

import ResolvedActionControls from '../ResolvedActionControls.svelte';


export default {
    title: 'acls/ResolvedActionControls',
};


export const ideal = () => ({
    Component: ResolvedActionControls,
    props: {
        action: AclAction.READ,
        actionResolvedAgents: mockActionResolvedAgents,
        agentsGraph: mockAgentsGraph,
    },
});
