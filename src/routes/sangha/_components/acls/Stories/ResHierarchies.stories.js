
import '../../../../../storybook-init';
import { mockResHierarchy, mockResGraph } from '../../../../../mock-data/acls';
import ResHierarchies from '../ResHierarchies.svelte';


export default {
    title: 'acls/ResHierarchies',
};


export const ideal = () => ({
    Component: ResHierarchies,
    props: {
        hierarchies: [mockResHierarchy],
        resGraph: mockResGraph,
    },
});

