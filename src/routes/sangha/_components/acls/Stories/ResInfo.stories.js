
import '../../../../../storybook-init';
import { mockResources } from '../../../../../mock-data/acls';
import ResInfo from '../ResInfo.svelte';


export default {
    title: 'acls/ResInfo',
};


export const ideal = () => ({
    Component: ResInfo,
    props: {
        res: mockResources.r1,
    },
});
