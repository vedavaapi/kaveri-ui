
import '../../../../../../storybook-init';
import { mockVC, mockUsersGraph, mockTeamsGraph, mockAgentsGraph, mockRes } from '../../../../../../mock-data/acls';
import AclUpdateFlow from '../AclUpdateFlow.svelte';


export default {
    title: 'acls/updater/AclUpdateFlow',
};


export const ideal = () => ({
    Component: AclUpdateFlow,
    props: {
        res: mockRes,
        agentsSet: {
            users: Object.keys(mockUsersGraph),
            teams: Object.keys(mockTeamsGraph).slice(0, 2),
        },
        agentsGraph: mockAgentsGraph,
        control: 'grant',
        vc: mockVC,
    },
});


export const openedToAgentsSelector = () => ({
    Component: AclUpdateFlow,
    props: {
        res: mockRes,
        agentsSet: {
            users: Object.keys(mockUsersGraph),
            teams: Object.keys(mockTeamsGraph).slice(0, 2),
        },
        agentsGraph: mockAgentsGraph,
        control: 'grant',
        vc: mockVC,
        openingScreen: 'agents-selector',
    },
});
