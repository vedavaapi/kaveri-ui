
import '../../../../../../storybook-init';
import { mockRes, mockUsersGraph, mockTeamsGraph, mockAgentsGraph } from '../../../../../../mock-data/acls';
import AclUpdater from '../AclUpdater.svelte';


export default {
    title: 'acls/updater/AclUpdater',
};


export const ideal = () => ({
    Component: AclUpdater,
    props: {
        res: mockRes,
        agentsSet: {
            users: Object.keys(mockUsersGraph),
            teams: Object.keys(mockTeamsGraph).slice(0, 2),
        },
        agentsGraph: mockAgentsGraph,
        control: 'grant',
    },
});
