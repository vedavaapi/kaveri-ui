import '../../../../../storybook-init';
import { mockTeams } from '../../../../../mock-data/acls';
import TeamUpserter from '../TeamUpserter.svelte';

export default {
    title: 'actions/TeamUpserter',
};

export const ideal = () => ({
    Component: TeamUpserter,
    props: {
        team: mockTeams.t1,
    },
});
