
import '../../../../../storybook-init';
import { mockVC } from '../../../../../mock-data/acls';
import PagesSequenceInspector from '../PagesSequenceInspector.svelte';

export default {
    title: 'actions/PagesSequenceInspector',
};

export const proper = () => ({
    Component: PagesSequenceInspector,
    props: {
        bookId: '5ec2256cd27e4b000e82acf8',
        vc: mockVC,
    },
});

export const noSeq = () => ({
    Component: PagesSequenceInspector,
    props: {
        bookId: '5daed8c431a0040009737871',
        vc: mockVC,
    },
});


export const test = () => ({
    Component: PagesSequenceInspector,
    props: {
        bookId: '5f137441f483db0007cd7639',
        vc: mockVC,
    },
});
