export function getLibraryFormSchema($_, mode) {
    return {
        title: $_('g.library'),
        submit: $_(`g.${mode}`),
        fields: [
            { name: 'source', label: $_('actions.upsert-lib.parent-lib'), type: '_id', allowedClasses: ['Library'] },
            { name: 'name', label: $_('actions.upsert-lib.name'), type: 'string', required: true },
            { name: 'description', label: $_('actions.upsert-lib.descr'), type: 'text' },
            { name: 'logo', label: $_('actions.upsert-lib.logo'), type: 'file', accept: 'image/*', protos: ['file', 'http'] },
        ],
    };
}
