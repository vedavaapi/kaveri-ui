export function getBookFormSchema($_, mode) {
    return {
        title: $_('g.book'),
        submit: $_(`g.${mode}`),
        fields: [
            { name: 'source', label: $_('actions.upsert-lib.parent-lib'), type: '_id', allowedClasses: ['Library'] },
            { name: 'title', label: $_('actions.upsert-book.title'), type: 'string', required: true },
            { name: 'description', label: $_('actions.upsert-lib.descr'), type: 'text' },
            { name: 'cover', label: $_('actions.upsert-book.cover'), type: 'file', accept: 'image/*', protos: ['file', 'http'] },
        ],
    };
}

export function getBookFromPdfFormSchema($_) {
    return {
        title: $_('g.book'),
        submit: $_('g.import'),
        fields: [
            { name: 'source', label: $_('actions.upsert-lib.parent-lib'), type: '_id', allowedClasses: ['Library'] },
            { name: 'title', label: $_('actions.upsert-book.title'), type: 'string', required: true },
            { name: 'description', label: $_('actions.upsert-lib.descr'), type: 'text' },
            { name: 'representations.stillImage.0', label: $_('actions.import-pdf.pdf-document'), type: 'file', accept: 'application/pdf', protos: ['file', 'http'], required: true },
            { name: 'is_scanned', label: $_('actions.import-pdf.is-scanned'), type: 'yn', ynLabels: { y: $_('g.yes'), n: $_('g.no') } },
        ],
    };
}

export function getBulkPdfImportFormSchema($_) {
    return {
        title: $_('g.books'),
        submit: $_('g.import'),
        fields: [
            { name: 'source', label: $_('actions.upsert-lib.parent-lib'), type: '_id', allowedClasses: ['Library'], required: true },
            { name: 'pdfs', label: $_('actions.bulk-import-pdfs.pdf-documents'), type: 'file', accept: 'application/pdf', protos: ['file', 'http'], multiple: true, required: true },
            { name: 'are_scanned', label: $_('actions.bulk-import-pdfs.are-scanned'), type: 'yn', ynLabels: { y: $_('g.yes'), n: $_('g.no') } },
        ],
    };
}
