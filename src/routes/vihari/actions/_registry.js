export const PrimaryActionsRegistry = {
    Library: {
        update: 'vihari/actions/update-library',
    },
    ScannedBook: {
        update: 'vihari/actions/update-book',
    },
    Team: {
        update: 'vihari/actions/update-team',
    },
};
