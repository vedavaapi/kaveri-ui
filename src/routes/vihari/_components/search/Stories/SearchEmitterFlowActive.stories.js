import '../../../../../storybook-init';
import { allClassNames, mockLC } from '../../../../../mock-data/vihari';
import SearchEmitterFlowActive from '../SearchEmitterFlowActive.svelte';


export default {
    title: 'vihari/search/SearchEmitterFlowActive',
};


export const ideal = () => ({
    Component: SearchEmitterFlowActive,
    props: {
        classNames: allClassNames,
        lc: mockLC,
    },
});
