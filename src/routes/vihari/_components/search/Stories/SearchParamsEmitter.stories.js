
import '../../../../../storybook-init';
import { sbssfs, sbasfs, mockSbFrMap2, mockSbFvMap2, mockSbFrMap3, mockSbFvMap3, mockSbFvlMap3, mockLC } from '../../../../../mock-data/vihari/search';
import SearchParamsEmitter from '../SearchParamsEmitter.svelte';


export default {
    title: 'vihari/search/SearchParamsEmitter',
};


export const ScannedBook = () => ({
    Component: SearchParamsEmitter,
    props: {
        simpleSearchSpecs: sbssfs,
        advancedSearchFieldSpecs: sbasfs,
        lc: mockLC,
        cmg: {
            and: {
                fvMap: mockSbFvMap2,
                frMap: mockSbFrMap2,
            },
            or: {

            },
        },
    },
});


export const ScannedBook2 = () => ({
    Component: SearchParamsEmitter,
    props: {
        simpleSearchSpecs: sbssfs,
        advancedSearchFieldSpecs: sbasfs,
        lc: mockLC,
        cmg: {
            and: {
                fvMap: mockSbFvMap3,
                frMap: mockSbFrMap3,
                fvlMap: mockSbFvlMap3,
            },
            or: {

            },
        },
    },
});
