
import '../../../../../../storybook-init';
import { sbssfs, mockLC } from '../../../../../../mock-data/vihari/search';
import SimpleSearchInput from '../SimpleSearchInput.svelte';


export default {
    title: 'vihari/search/simple/SimpleSearchInput',
};


export const ideal = () => ({
    Component: SimpleSearchInput,
    props: {
        simpleSearchSpecs: sbssfs,
        lc: mockLC,
    },
});
