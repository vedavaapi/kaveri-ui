
import '../../../../../../storybook-init';
import { sbssfs, mockLC, mockSbFrMap2, mockSbFvMap2 } from '../../../../../../mock-data/vihari/search';
import SimpleSearchParamsEmitter from '../SimpleSearchParamsEmitter.svelte';


export default {
    title: 'vihari/search/simple/SimpleSearchParamsEmitter',
};


export const ideal = () => ({
    Component: SimpleSearchParamsEmitter,
    props: {
        simpleSearchSpecs: sbssfs,
        lc: mockLC,
        cmg: {
            and: {
                fvMap: mockSbFvMap2,
                frMap: mockSbFrMap2,
            },
            or: {

            },
        },
    },
});
