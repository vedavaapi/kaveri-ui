
import '../../../../../../storybook-init';
import { sbasfs, lasfs, mockLC } from '../../../../../../mock-data/vihari/search';
import AdvancedSearchInput from '../AdvancedSearchInput.svelte';


export default {
    title: 'vihari/search/AdvancedSearchInput',
};


export const ScannedBook = () => ({
    Component: AdvancedSearchInput,
    props: {
        advancedSearchFieldSpecs: sbasfs,
        lc: mockLC,
    },
});


export const Library = () => ({
    Component: AdvancedSearchInput,
    props: {
        advancedSearchFieldSpecs: lasfs,
        lc: mockLC,
    },
});
