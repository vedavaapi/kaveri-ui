
import '../../../../../../storybook-init';
import { sbasfs, mockSbFrMap2, mockSbFvMap2, mockSbFrMap3, mockSbFvMap3, mockSbFvlMap3, mockLC } from '../../../../../../mock-data/vihari/search';
import AdvancedSearchParamsEmitter from '../AdvancedSearchParamsEmitter.svelte';


export default {
    title: 'vihari/search/advanced/AdvancedSearchParamsEmitter',
};


export const ScannedBook = () => ({
    Component: AdvancedSearchParamsEmitter,
    props: {
        advancedSearchFieldSpecs: sbasfs,
        lc: mockLC,
        cmg: {
            and: {
                fvMap: mockSbFvMap2,
                frMap: mockSbFrMap2,
            },
            or: {

            },
        },
    },
});


export const ScannedBook2 = () => ({
    Component: AdvancedSearchParamsEmitter,
    props: {
        advancedSearchFieldSpecs: sbasfs,
        lc: mockLC,
        cmg: {
            and: {
                fvMap: mockSbFvMap3,
                frMap: mockSbFrMap3,
                fvlMap: mockSbFvlMap3,
            },
            or: {

            },
        },
    },
});
