
import '../../../../../../storybook-init';
import { allClassNames, mockLC } from '../../../../../../mock-data/vihari';
import JsonClassSearchInput from '../JsonClassSearchInput.svelte';


export default {
    title: 'vihari/search/jc/JsonClassSearchInput',
};


export const ideal = () => ({
    Component: JsonClassSearchInput,
    props: {
        classNames: allClassNames,
        lc: mockLC,
    },
});
