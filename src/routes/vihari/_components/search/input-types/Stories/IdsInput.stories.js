
import '../../../../../../storybook-init';
import IdsInput from './IdsInput.view.svelte';


export default {
    title: 'vihari/search/in/IdsInput',
};


export const Single = () => ({
    Component: IdsInput,
    props: {
        allowedClassNames: ['ScannedBook'],
    },
});


export const Multi = () => ({
    Component: IdsInput,
    props: {
        allowedClassNames: ['ScannedBook'],
        mode: 'multi',
    },
});


export const SingleSupplied = () => ({
    Component: IdsInput,
    props: {
        allowedClassNames: ['ScannedBook'],
        resId: '5d7df8c16864390012210d5e',
    },
});


export const MultiSupplied = () => ({
    Component: IdsInput,
    props: {
        mode: 'multi',
        allowedClassNames: ['ScannedBook'],
        resIds: ['5d7df8c16864390012210d5e', '5d62061e12820900114cd94d'],
    },
});
