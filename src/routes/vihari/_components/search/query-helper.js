import escapeRegexp from 'escape-string-regexp';

export const getFieldLabel = (fs, lc) => {
    const flt = lc.getLocalizedText(fs.label);
    return flt ? flt.chars : fs.field;
};

export const fieldTypes = ['number', 'string', '_id', 'bool'];

export const fieldSupportedRelationsMap = {
    number: ['is', 'isNot', 'gt', 'lt'], // TODO exists, nexists relations without object.
    string: ['has', 'is', 'isNot', 'sw', 'ew'],
    _id: ['is', 'in'],
    bool: ['is', 'isNot'],
    enum: ['in', 'is', 'isNot', 'notIn'],
    agent_id: ['is', 'in', 'notIn'],
    time: ['inRange'],
};

/**
 *
 * @param {Object} fvMap
 * @param {Object} frMap
 */
export function getQuery(fvMap, frMap, ignoreEmptyInput = true) {
    const query = {};
    Object.keys(fvMap).forEach((f) => {
        const v = fvMap[f];
        if (v === '' && ignoreEmptyInput) return;
        const r = frMap[f] || 'is';
        let qi;
        if (r === 'is') {
            qi = v;
        } else if (r === 'isNot') {
            qi = { $ne: v };
        } else if (r === 'has') {
            qi = { $regex: escapeRegexp(v), $options: 'ix' };
        } else if (r === 'sw') {
            qi = { $regex: `^${escapeRegexp(v)}`, $options: 'ix' };
        } else if (r === 'ew') {
            qi = { $regex: `${escapeRegexp(v)}$`, $options: 'ix' };
        } else if (r === 'gt') {
            qi = { $gt: v };
        } else if (r === 'lt') {
            qi = { $lt: v };
        } else if (r === 'in') {
            if (Array.isArray(v) && v.length) {
                qi = { $in: v };
            }
        } else if (r === 'notIn') {
            if (Array.isArray(v) && v.length) {
                qi = { $not: { $in: v } };
            }
        } else if (r === 'inRange') {
            if (v && Object.keys(v).length) {
                qi = { $gte: v.from, $lte: v.to };
            }
        }
        // console.log({ f, r, v, qi });
        if (qi) {
            query[f] = qi;
        }
    });
    // console.log({ query });
    return query;
}

export function getAllFields(mapsGroup) {
    let fields = [];
    ['or', 'and'].forEach((conj) => {
        const mgc = mapsGroup[conj];
        if (!mgc) return;
        const { fvMap } = mgc;
        if (!fvMap) return;
        fields = [...fields, ...Object.keys(fvMap)];
    });
    return new Set(fields);
}

export function constraintsToMapsGroup(constraints) {
    return {
        and: {
            fvMap: constraints,
        },
    };
}

/**
 * to proprly manage simple(s)/advanced(a) query items, with few constant(c) pre set query items
 * we will choose one among advanced/simple, and merge those query docs with contant pre sets to get final query doc.
 * we have to consider conjunctions too.
 */
export class QueryManager {
    constructor() {
        this._mapGroups = {};
        this.getMapGroup('c');
    }

    getMapGroup(groupName) {
        if (!this._mapGroups[groupName]) {
            this._mapGroups[groupName] = {
                and: { fvMap: {}, frMap: {}, fvlMap: {} },
                or: { fvMap: {}, frMap: {}, fvlMap: {} },
            };
        }
        return this._mapGroups[groupName];
    }

    _mergeMaps(groupName, conj, maps, replace) {
        const mgc = this.getMapGroup(groupName)[conj];
        if (!mgc) return;
        Object.keys(mgc).forEach((mapName) => {
            if (replace) mgc[mapName] = {};
            const update = maps[mapName];
            if (!update) return;
            Object.assign(mgc[mapName], update);
        });
    }

    _mergeMapsGroup(groupName, mg, replace) {
        ['and', 'or'].forEach((conj) => {
            this._mergeMaps(groupName, conj, mg[conj] || {}, replace);
        });
    }

    mergeToConstantGroup(mg, replace = false) {
        this._mergeMapsGroup('c', mg, replace);
    }

    mergeToAdvancedGroup(mg, replace = false) {
        this._mergeMapsGroup('a', mg, replace);
    }

    mergeToSimpleGroup(mg, replace = false) {
        this._mergeMapsGroup('s', mg, replace);
    }

    getConstantFields() {
        const cmg = this.getMapGroup('c');
        return getAllFields(cmg);
    }

    getResultantQuery(ignoreEmptyInput = true) {
        const vmg = this._mapGroups.a || this._mapGroups.s; // select advanced, if not then simple
        const cmg = this.getMapGroup('c');

        const conjQMap = { and: {}, or: {} };
        ['and', 'or'].forEach((conj) => {
            const q = conjQMap[conj];
            if (vmg) {
                Object.assign(q, getQuery(vmg[conj].fvMap, vmg[conj].frMap, ignoreEmptyInput));
            }
            if (cmg) {
                Object.assign(q, getQuery(cmg[conj].fvMap, cmg[conj].frMap, ignoreEmptyInput));
            }
        });
        const combinedQuery = conjQMap.and;
        if (Object.keys(conjQMap.or).length) {
            combinedQuery.$or = [];
            Object.keys(conjQMap.or).forEach((k) => {
                combinedQuery.$or.push({ [k]: conjQMap.or[k] });
            });
        }
        return combinedQuery;
    }
}
