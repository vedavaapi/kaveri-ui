
import '../../../../../../storybook-init';
import { book1Repr, book2Repr } from '../../../../../../mock-data/vihari';
import QueryItemRepr from '../QueryItemRepr.svelte';


export default {
    title: 'vihari/search/constant/QueryItemRepr',
};


export const Book1NameQI = () => ({
    Component: QueryItemRepr,
    props: {
        valueLabel: book1Repr.name,
        relation: 'sw',
        fieldLabel: 'name',
    },
});


export const Book2NameQI = () => ({
    Component: QueryItemRepr,
    props: {
        valueLabel: book2Repr.name,
        relation: 'sw',
        fieldLabel: 'name',
    },
});
