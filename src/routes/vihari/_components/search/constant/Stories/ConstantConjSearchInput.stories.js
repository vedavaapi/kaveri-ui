
import '../../../../../../storybook-init';
import { sbafsMap, mockSbFrMap, mockSbFvMap, mockSbFvlMap, mockLC } from '../../../../../../mock-data/vihari/search';
import ConstantConjSearchInput from '../ConstantConjSearchInput.svelte';


export default {
    title: 'vihari/search/constant/ConstantConjSearchInput',
};


export const ideal = () => ({
    Component: ConstantConjSearchInput,
    props: {
        fsMap: sbafsMap,
        frMap: mockSbFrMap,
        fvMap: mockSbFvMap,
        fvlMap: mockSbFvlMap,
        lc: mockLC,
    },
});
