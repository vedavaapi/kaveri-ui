
import '../../../../../../storybook-init';
import { sbasfs, mockSbFrMap, mockSbFvMap, mockSbFvlMap, mockLC } from '../../../../../../mock-data/vihari/search';
import ConstantSearchInput from '../ConstantSearchInput.svelte';


export default {
    title: 'vihari/search/constant/ConstantSearchInput',
};


export const ideal = () => ({
    Component: ConstantSearchInput,
    props: {
        fieldSpecs: sbasfs,
        lc: mockLC,
        mapsGroup: { and: {
            fvMap: mockSbFvMap,
            frMap: mockSbFrMap,
            fvlMap: mockSbFvlMap,
        },
        or: {

        } },
    },
});
