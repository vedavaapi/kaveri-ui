import { LocaleContext } from '@vedavaapi/jsonld-helpers/dist/locale';
// eslint-disable-next-line no-unused-vars
import { Context } from '@vedavaapi/client';
import { get } from 'svelte/store';
import { _ } from 'svelte-i18n';

// eslint-disable-next-line no-unused-vars
import { UiProfileStore } from '@vedavaapi/classdefs/dist/stores';
import { UiCompiliedProfileTemplate } from '@vedavaapi/classdefs/dist/models';
// eslint-disable-next-line no-unused-vars
import { IDotTemplateData } from '@vedavaapi/classdefs/dist/types';

import { ResourceReprAccessor } from './accessors';

/** for static factory methods */
import { initClassdefsStores, uiProfileStoreSq } from '../../../../classdefs-stores';
import { lc as lcStore, vc as vcStore } from '../../../../context-stores';

export class UiProfilesManager {
    /**
     *
     * @param {{ upStore: UiProfileStore, lc: LocaleContext, vc: Context }} param0
     */
    constructor({ upStore, lc, vc }) {
        this.upStore = upStore;
        this.lc = lc || new LocaleContext({});
        this.vc = vc;
        this._ = get(_);

        /**
         * @type Object<string, UiCompiliedProfileTemplate>
         */
        this.profilesMap = {};
        /**
         * @type Object<string, ResourceReprAccessor>
         */
        this.accessorsMap = {};

        this.defaultProfile = new UiCompiliedProfileTemplate({ uiProfile: {} });
        this.defaultAccessor = new ResourceReprAccessor({ lc: this.lc, vc: this.vc, _: this._ });
    }

    resetStores({ upStore }) {
        if (upStore) this.upStore = upStore;
    }

    reComputeAccessors() {
        Object.keys(this.accessorsMap).forEach((className) => {
            const oldAccessor = this.accessorsMap[className];
            this.accessorsMap[className] = new ResourceReprAccessor({
                clsProfile: oldAccessor._cls,
                vc: this.vc,
                lc: this.lc,
                _: this._,
            });
        });
    }

    resetContexts({ lc, vc, reComputeAccessors = true }) {
        if (lc) this.lc = lc;
        if (vc) this.vc = vc;
        this.defaultAccessor = new ResourceReprAccessor({ lc: this.lc, vc: this.vc, _: this._ });
        if (reComputeAccessors) this.reComputeAccessors();
    }

    async ensureFor(resources) {
        if (!resources) return;
        const newClassNames = new Set();
        resources.forEach((res) => {
            const className = res.jsonClass;
            if (!className) return;
            if (this.profilesMap[className]) return;
            newClassNames.add(className);
        });
        let newUiProfilesMap;
        try {
            newUiProfilesMap = await this.upStore.getMany([...newClassNames], this.vc);
        } catch (error) {
            newUiProfilesMap = {};
            console.log('error in getting ui profiles map', error); // TODO
        }
        newClassNames.forEach((clsName) => {
            const profile = newUiProfilesMap[clsName] || this.defaultProfile;
            this.profilesMap[clsName] = profile;
            this.accessorsMap[clsName] = new ResourceReprAccessor({
                clsProfile: profile.repr.cls, lc: this.lc, vc: this.vc, _: this._,
            });
        });
    }

    static async initForResources(resources) {
        await initClassdefsStores(get(vcStore), 3);
        const pm = new UiProfilesManager({ upStore: get(uiProfileStoreSq), lc: get(lcStore), vc: get(vcStore) });
        await pm.ensureFor(resources);
        return pm;
    }

    /**
     * 
     * @param {Array<any>} resources
     * @param {Partial<IDotTemplateData>} reprTemplateData;
     */
    static async getReprsAndAccessors(resources, reprTemplateData = {}) {
        const pm = await UiProfilesManager.initForResources(resources);
        const reprs = resources.map((res) => {
            const templateData = { ...reprTemplateData, _: res };
            return pm.profilesMap[res.jsonClass].repr.instance(templateData);
        });
        return { reprs, accessorsMap: pm.accessorsMap };
    }
}
