/* eslint-disable max-classes-per-file */
// eslint-disable-next-line no-unused-vars
import { IResourceResolvedRepr, IUiReprCls } from '@vedavaapi/classdefs/dist/types';
import { LocaleContext } from '@vedavaapi/jsonld-helpers/dist/locale';
// eslint-disable-next-line no-unused-vars
import { Context, objstore, iiifi } from '@vedavaapi/client';
import { ooldId as getOOLDId } from '@vedavaapi/types/dist/DataRepresentation';
// import { supportsIIIF as doesSupportsIIIF } from '@vedavaapi/types/dist/StillImageRepresentation';

// eslint-disable-next-line import/no-cycle
import { marshalFieldValueToLocaleTextOptions } from './field-marshals';

export class ResourceView {
    /**
     *
     * @param {{ _id: string, name: string, classLabel: string; by?: string[]; description?: string; imgReprUrl: string; txtRepr?: string }} param0
     */
    constructor({ _id, name, classLabel, by, description, imgReprUrl, txtRepr }) {
        this._id = _id;
        this.name = name;
        this.classLabel = classLabel;
        this.by = by;
        this.description = description;
        this.imgReprUrl = imgReprUrl;
        this.txtRepr = txtRepr;
    }
}

export class ResourceReprAccessor {
    /**
     *
     * @param {{ clsProfile?: IUiReprCls, lc?: LocaleContext, vc?: Context, _?: any }} param0
     */
    constructor({ clsProfile, lc, vc, _ }) {
        this._cls = clsProfile || {};

        this._lc = lc || new LocaleContext({});
        /**
         * @private
         */
        this._vc = vc;
        /**
         * @private
         */
        this._classLabel = (this._lc.getLocalizedText(this._cls.label) || {}).chars;
        this._ = _; // formatter, if we want to use app-messages
    }

    /**
     * @param {IResourceResolvedRepr} repr
     */
    classLabel(repr) {
        return this._classLabel || repr.jsonClassLabel || repr.jsonClass;
    }

    /**
     * @param {IResourceResolvedRepr} repr
     */
    name(repr) {
        const name = this._lc.getLocalizedText(marshalFieldValueToLocaleTextOptions(repr.name, this, repr));
        // return name ? name.chars : `${this.classLabel(repr)}: ${repr._id}`;
        return name ? name.chars : repr._id;
    }

    /**
     *
     * @param {IResourceResolvedRepr} repr
     */
    // eslint-disable-next-line class-methods-use-this
    by(repr) {
        return repr.by;
    }

    /**
     *
     * @param {IResourceResolvedRepr} repr
     */
    // eslint-disable-next-line class-methods-use-this
    _id(repr) {
        return repr._id;
    }

    /**
     *
     * @param {IResourceResolvedRepr} repr
     */
    description(repr) {
        const description = this._lc.getLocalizedText(repr.description);
        return description ? description.chars : undefined;
    }

    /**
     *
     * @param {IResourceResolvedRepr} repr
     */
    imgReprUrl(repr) {
        const stillImageRepr = repr.img_repr;
        // if (!stillImageRepr) return `clsicons/${repr.jsonClass}.png`;
        if (!stillImageRepr) return undefined;
        if (typeof stillImageRepr === 'string') return stillImageRepr;

        const ooldId = getOOLDId(stillImageRepr);
        // const supportsIIIF = doesSupportsIIIF(stillImageRepr);
        const supportsIIIF = true; // Now all images support iiif

        if (!supportsIIIF && !repr.img_repr_selector) return objstore.Files.url({ vc: this._vc, params: { file_id: ooldId } });
        const region = repr.img_repr_selector ? repr.img_repr_selector.value.replace(/xywh *= */, '') : 'full';
        return iiifi.Image.url({
            vc: this._vc,
            params: {
                service: 'objstore',
                ident: ooldId,
                region,
                size: '200,',
                rotation: '0',
                quality: 'default',
                format: 'jpg',
            },
        });
    }

    /**
     *
     * @param {IResourceResolvedRepr} repr
     */
    // eslint-disable-next-line class-methods-use-this
    txtRepr(repr) {
        const txt = repr.txt_repr;
        if (txt === undefined || txt === null) return undefined;
        if (typeof txt === 'string') return txt;
        const localizedTxt = this._lc.getLocalizedText(txt);
        return localizedTxt ? localizedTxt.chars : undefined;
    }

    /**
     * @param {IResourceResolvedRepr} repr
     * @param {string[]} props
     * @param {boolean} include
     */
    view(repr, props = null, include = true) {
        const v = {};
        // eslint-disable-next-line no-param-reassign
        props = props || this.allProps;
        this.allProps.forEach((prop) => {
            if (!!include !== !!props.includes(prop)) return;
            v[prop] = this[prop](repr);
        });
        // @ts-ignore
        return new ResourceView(v);
    }
}

ResourceReprAccessor.prototype.allProps = ['_id', 'name', 'classLabel', 'by', 'description', 'imgReprUrl', 'txtRepr'];
