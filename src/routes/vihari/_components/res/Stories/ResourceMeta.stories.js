
import '../../../../../storybook-init';
import { book1Repr, book2Repr, bookAccessor } from '../../../../../mock-data/vihari';
import ResourceMeta from '../ResourceMeta.svelte';


export default {
    title: 'vihari/res/ResourceMeta',
};


export const Book1Meta = () => ({
    Component: ResourceMeta,
    props: {
        repr: book1Repr,
        accessor: bookAccessor,
    },
});


export const Book2Meta = () => ({
    Component: ResourceMeta,
    props: {
        repr: book2Repr,
        accessor: bookAccessor,
    },
});
