
import '../../../../../storybook-init';
import { book1Repr, book2Repr, bookAccessor, library1Repr, libraryAccessor } from '../../../../../mock-data/vihari';
import ResourceRepr from './ResourceRepr.view.svelte';

export default {
    title: 'vihari/res/ResourceRepr',
};

const library1View = libraryAccessor.view(library1Repr);

export const Book1 = () => ({
    Component: ResourceRepr,
    props: {
        repr: book1Repr,
        accessor: bookAccessor,
        parentView: library1View,
    },
});


export const Book1WithoutSelect = () => ({
    Component: ResourceRepr,
    props: {
        repr: book1Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { memaction: true },
    },
});


export const Book1WithoutMeta = () => ({
    Component: ResourceRepr,
    props: {
        repr: book1Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { infoMeta: true },
    },
});


export const Book1Simple = () => ({
    Component: ResourceRepr,
    props: {
        repr: book1Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { info: true },
    },
});


export const Book1SimpleMember = () => ({
    Component: ResourceRepr,
    props: {
        repr: book1Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { info: true },
        membershipAction: 'remove',
    },
});


export const Book1SimpleWithoutSelect = () => ({
    Component: ResourceRepr,
    props: {
        repr: book1Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { memaction: true, info: true },
    },
});


export const Book2 = () => ({
    Component: ResourceRepr,
    props: {
        repr: book2Repr,
        accessor: bookAccessor,
        parentView: library1View,
    },
});


export const Book2SimpleWithoutSelect = () => ({
    Component: ResourceRepr,
    props: {
        repr: book2Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { memaction: true, info: true },
    },
});


export const Book2WithoutMeta = () => ({
    Component: ResourceRepr,
    props: {
        repr: book2Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { infoMeta: true },
    },
});


export const Book2Simple = () => ({
    Component: ResourceRepr,
    props: {
        repr: book2Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { info: true },
    },
});


export const Book2WithoutSelect = () => ({
    Component: ResourceRepr,
    props: {
        repr: book2Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { memaction: true },
    },
});


export const Book2SimpleMember = () => ({
    Component: ResourceRepr,
    props: {
        repr: book2Repr,
        accessor: bookAccessor,
        parentView: library1View,
        hideFlags: { info: true },
        membershipAction: 'remove',
    },
});


export const Library1 = () => ({
    Component: ResourceRepr,
    props: {
        repr: library1Repr,
        accessor: libraryAccessor,
    },
});


export const Library1WithoutSelect = () => ({
    Component: ResourceRepr,
    props: {
        repr: library1Repr,
        accessor: libraryAccessor,
        hideFlags: { memaction: true },
    },
});


export const Library1WithoutMeta = () => ({
    Component: ResourceRepr,
    props: {
        repr: library1Repr,
        accessor: libraryAccessor,
        hideFlags: { infoMeta: true },
    },
});


export const Library1Simple = () => ({
    Component: ResourceRepr,
    props: {
        repr: library1Repr,
        accessor: libraryAccessor,
        hideFlags: { info: true },
    },
});


export const Library1SimpleWithoutSelect = () => ({
    Component: ResourceRepr,
    props: {
        repr: library1Repr,
        accessor: libraryAccessor,
        hideFlags: { memaction: true, info: true },
    },
});
