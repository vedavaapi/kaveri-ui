// eslint-disable-next-line
import { ResourceReprAccessor } from './accessors';

/**
 *
 * @param {*} selector
 * @param {ResourceReprAccessor} accessor
 */
function marshalFragmentSelector(selector, accessor) {
    let { value } = selector;
    if (!value) return null;
    value = value.substring(5, value.length).replace(/,/g, ', ');

    return `${accessor._ ? accessor._('selectors.bbox', { locale: accessor._lc.locale }) : 'BBox'}: ${value}`;
}

/**
 *
 * @param {*} selector
 * @param {ResourceReprAccessor} accessor
 */
function marshalIndexSelector(selector, accessor, repr) {
    const { index } = selector;
    if (!index) return null;
    return `${accessor.classLabel(repr)}: ${index}`;
}

/**
 *
 * @param {any} v;
 * @param {ResourceReprAccessor} accessor
 */
export function marshalFieldValueToLocaleTextOptions(v, accessor, repr) {
    // don't go into infinite loop.
    if (!v || Array.isArray(accessor) || typeof v !== 'object') return v;
    if (v.jsonClass === 'Text') return v;
    if (v.jsonClass === 'FragmentSelector') return marshalFragmentSelector(v, accessor);
    if (v.jsonClass === 'IndexSelector') return marshalIndexSelector(v, accessor, repr);
    return null;
}
