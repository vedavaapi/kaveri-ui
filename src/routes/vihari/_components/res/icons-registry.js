export const IconsRegistry = {
    Library: 'library',
    ScannedBook: 'book-reference',
    ScannedPage: 'document',
    User: 'user',
    Team: 'user-group',
    TextAnnotation: 'conversation',
    ImageRegion: 'edit-crop',
    SequenceAnnotation: 'list-bullet',
};
