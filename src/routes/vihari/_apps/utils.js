// eslint-disable-next-line no-unused-vars
import { IPlainObject } from '@vedavaapi/types/dist/base';


/**
 * @param {IPlainObject} clsPropMap
 * @param {string} clsName
 * @param { string[] } inheritanceBranch
 */
export function getInheritedProp(clsPropMap, clsName, inheritanceBranch) {
    let matchedProp = clsPropMap[clsName];
    if (matchedProp) return matchedProp;

    inheritanceBranch.some((ancestorName) => {
        const prop = clsPropMap[ancestorName];
        if (prop) {
            matchedProp = prop;
        }
        return !!prop;
    });
    return matchedProp;
}
