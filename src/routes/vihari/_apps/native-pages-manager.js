// eslint-disable-next-line no-unused-vars
import { IResource } from '@vedavaapi/types/dist/Resource';

import NativePagesRegistry from '../../../default_pages.json';
import { getInheritedProp } from './utils';


export const NativeAppManager = {
    /**
     *
     * @param {{ res: IResource, inheritanceBranch: string[] }} param0
     */
    getPageUrl({ res, inheritanceBranch }) {
        const clsPath = getInheritedProp(NativePagesRegistry, res.jsonClass, inheritanceBranch) || 'vihari/res';
        return `${clsPath}/${res._id}`;
    },
};
