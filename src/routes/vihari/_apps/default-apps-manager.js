// eslint-disable-next-line no-unused-vars
import { IResource } from '@vedavaapi/types/dist/Resource';
import { URLSearchParams } from '@vedavaapi/web';

import DefaultAppsRegistry from '../../../default_apps.json';
import { getInheritedProp } from './utils';


export const DefaultAppsManager = {
    /**
     *
     * @param {{ res: IResource, inheritanceBranch: string[] }} param0
     */
    getApp({ res, inheritanceBranch }) {
        const app = getInheritedProp(DefaultAppsRegistry, res.jsonClass, inheritanceBranch);
        if (!app) return undefined;
        const qParams = new URLSearchParams({
            _id: res._id,
            jsonClass: res.jsonClass,
            kaveri: document.querySelector('base').href,
        });
        return {
            name: app.name,
            url: `${app.url}?${qParams.toString()}`,
        };
    },
};
