import { objstore } from '@vedavaapi/client';


export async function getResourceLoad(vc, resId) {
    const resp = await objstore.Graph.put({
        vc,
        data: {
            direction: 'referred',
            start_nodes_selector: { _id: resId },
            traverse_key_filter_maps_list: [
                {
                    source: {},
                },
            ],
            json_class_projection_map: { '*': { content: 0 } },
            max_hops: 1,
            include_incomplete_paths: true,
        },
    });
    const gResp = resp.data;
    const res = gResp.graph[resId] || null;
    const parents = res ? gResp.getReachedResources(res, 'source') : [];
    return {
        res,
        parent: parents.length ? parents[0] : null,
    };
}
