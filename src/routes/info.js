
export async function get(req, res) {
    res.json({
        'app.mountpath': req.app.mountpath,
        'req.url': req.url,
        'req.originalUrl': req.originalUrl,
        'req.baseUrl': req.baseUrl,
        host: req.get('host'),
        'req.hostname': req.hostname,
        'req.ip': req.ip,
        'req.ips': req.ips,
        'req.path': req.path,
        'req.protocol': req.protocol,
        'req.query': req.query,
        fullUrl: `${req.protocol}://${req.get('host')}${req.originalUrl}`,
    });
}
