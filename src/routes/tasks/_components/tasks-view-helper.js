// eslint-disable-next-line no-unused-vars
import { tasker, objstore, Context, accounts } from '@vedavaapi/client';
import { AclAgentType } from '@vedavaapi/acls/dist/agent-types';

/**
 *
 * @param {tasker.manager.ITask[]} tasks
 */
export function collectTargetsAndAgents(tasks) {
    const targetIds = new Set();
    const userIds = new Set();
    const teamIds = new Set();
    tasks.forEach((task) => {
        targetIds.add(task.on.target.id);
        if (task.on.path) {
            task.on.path.forEach((item) => {
                targetIds.add(item.target.id);
            });
        }
        userIds.add(task.agent.user.id);
        if (task.agent.teams) {
            task.agent.teams.forEach((team) => {
                teamIds.add(team.id);
            });
        }
    });
    return { targetIds: [...targetIds], userIds: [...userIds], teamIds: [...teamIds] };
}

/**
 * @param { Context } vc;
 * @param {string[]} targetIds
 */
export async function getTargets(vc, targetIds) {
    const targetsGraph = {};
    try {
        const resp = await objstore.Resources.get({
            vc,
            params: {
                selector_doc: {
                    _id: { $in: targetIds },
                },
                projection: { resolvedPermissions: 0, hierarchy: 0, content: 0 },
            },
        });
        resp.data.items.forEach((res) => {
            targetsGraph[res._id] = res;
        });
    } catch (error) {
        //
    }

    const targets = [];
    targetIds.forEach((id) => {
        targets.push(targetsGraph[id] || { _id: id });
    });
    return targets;
}

/**
 *
 * @param {Context} vc
 * @param {string[]} agentIds
 * @param {AclAgentType} agentsType
 */
export async function getAgents(vc, agentIds, agentsType) {
    const agentsGraph = {};
    let agentsPromise;
    const params = {
        selector_doc: {
            _id: { $in: agentIds },
        },
        projection: { hierarchy: 0, resolvedPermissions: 0, externalAuthentications: 0 },
    };

    if (agentsType === AclAgentType.USERS) {
        // @ts-ignore
        agentsPromise = accounts.users.Users.get({ vc, params });
    } else {
        // @ts-ignore
        agentsPromise = accounts.teams.Teams.get({ vc, params });
    }

    try {
        const resp = await agentsPromise;
        resp.data.items.forEach((agent) => {
            agentsGraph[agent._id] = agent;
        });
    } catch (error) {
        //
    }

    agentIds.forEach((agentId) => {
        if (!agentsGraph[agentId]) {
            agentsGraph[agentId] = {
                jsonClass: agentsType === AclAgentType.USERS ? 'User' : 'Team',
                _id: agentId,
            };
        }
    });

    return agentsGraph;
}
