import MeSurface from './MeSurface.view.svelte';
import '../../../storybook-init';
import { user, userWithoutImage } from '../../../mock-data/auth';


export default {
    title: 'header/MeSurface',
};


export const withImage = () => ({
    Component: MeSurface,
    props: {
        user,
    },
});

export const withoutImage = () => ({
    Component: MeSurface,
    props: {
        user: userWithoutImage,
    },
});
