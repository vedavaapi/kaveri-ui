import '../../../storybook-init';
import Tabs from '../Tabs.svelte';


export default {
    title: 'global/Tabs',
};


export const Ideal = () => ({
    Component: Tabs,
    props: {
        tabSpecs: [
            { id: '1', label: 'Tab1' },
            { id: '2', label: 'Tab2' },
            { id: '3', label: 'Tab3' },
            { id: '4', label: 'Tab4' },
        ],
    },
});
