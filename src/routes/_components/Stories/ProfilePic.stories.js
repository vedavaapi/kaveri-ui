import ProfilePic from './ProfilePic.view.svelte';
import '../../../storybook-init';
import { user, userWithoutImage, team } from '../../../mock-data/auth';


export default {
    title: 'header/ProfilePic',
};


export const UserWithImage = () => ({
    Component: ProfilePic,
    props: {
        agent: user,
    },
});

export const UserWithoutImage = () => ({
    Component: ProfilePic,
    props: {
        agent: userWithoutImage,
    },
});


export const teamWithOutImage = () => ({
    Component: ProfilePic,
    props: {
        agent: team,
    },
});
