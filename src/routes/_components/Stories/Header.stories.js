import '../../../storybook-init';
import { unauthorizedSession, authorizedSession } from '../../../mock-data/auth';
import HeaderView from '../HeaderView.svelte';


export default {
    title: 'header/Header',
};


export const unauthorized = () => ({
    Component: HeaderView,
    props: {
        session: unauthorizedSession,
    },
});


export const authorized = () => ({
    Component: HeaderView,
    props: {
        session: authorizedSession,
    },
});
