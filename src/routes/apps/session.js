// @ts-ignore
import site from '../../site.json';
import { allowRequestedOriginWithCredsMiddleware } from '../../cors-middlewares';

import { getmatchedAppId } from './_helpers';

export async function get(req, res) {
    allowRequestedOriginWithCredsMiddleware(req, res, () => {
        const authorizedApps = req.apps || {};
        const referrer = req.get('Referer');
        // console.log({ referrer });
        if (!referrer) {
            res.status(403).json({
                error: 'invalid request',
            });
            return;
        }
        const matchedAppId = getmatchedAppId(authorizedApps, referrer, req.hostname);
        if (!matchedAppId) {
            res.status(401).json({
                error: 'app not authorized',
            });
            return;
        }
        res.status(200).json({
            site: site.url,
            session: req.ksession.session,
        });
    });
}
