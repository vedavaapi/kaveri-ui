/* eslint-disable no-underscore-dangle */
import { URL } from 'url';
import { APPS_COOKIE_NAME } from '../../names';
import { urlencodedBodyParser } from '../../body-parser-middlewares';
import { isURLError } from './_helpers';

export async function get(req, res) {
    const authorizedApps = req.apps || {};
    res.json({
        apps: authorizedApps,
    });
}

export async function post(req, res) {
    urlencodedBodyParser(req, res, () => {
        const apps = JSON.parse(req.body.apps) || [];
        const validApps = {};
        apps.forEach((app) => {
            try {
                const path = (app.path && new URL(app.path).href) || new URL(app.url).origin;
                validApps[app._id] = path;
            } catch (e) {
                if (isURLError(e)) {
                    // app.path or app.url is invalid
                } else {
                    throw e;
                }
            }
        });
        res.cookie(APPS_COOKIE_NAME, validApps, { signed: true });
        res.json({
            status: 'SUCCESS',
            authorizedAppIds: Object.keys(validApps),
        });
    });
}

export async function del(req, res) {
    urlencodedBodyParser(req, res, () => {
        const authorizedApps = req.apps || {};
        const appsToUnAuthorize = JSON.parse(req.body.apps) || [];
        appsToUnAuthorize.forEach((app) => {
            if (!Object.prototype.hasOwnProperty.call(authorizedApps, app._id)) {
                return;
            }
            delete authorizedApps[app._id];
        });
        res.cookie(APPS_COOKIE_NAME, authorizedApps, { signed: true });
        res.json({
            status: 'SUCCESS',
        });
    });
}
