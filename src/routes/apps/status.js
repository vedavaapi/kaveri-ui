import { getmatchedAppId } from './_helpers';
/**
 * we could also ask apps to pass their _id to embedded page, so that it can send us appId directly
 */
export async function get(req, res) {
    const { appURL } = req.query;
    const authorizedApps = req.apps || {};

    const matchedAppId = getmatchedAppId(authorizedApps, appURL, req.hostname);

    if (!matchedAppId) {
        res.status(404).json({
            error: 'app is not found in authorized apps',
        });
        return;
    }

    const appPath = authorizedApps[matchedAppId];
    res.status(200).json({
        _id: matchedAppId,
        path: appPath,
    });
}
