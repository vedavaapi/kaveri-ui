import { URL } from 'url';
import config from 'config';

export const isURLError = (e) => (e instanceof TypeError && e.name === 'TypeError [ERR_INVALID_URL]');

export function getmatchedAppId(authorizedApps, appURL, selfHostName = null) {
    if (!appURL) {
        return null;
    }
    let matchedAppId;
    Object.keys(authorizedApps).some((appId) => {
        const appPath = authorizedApps[appId];
        if (appURL.startsWith(appPath)) {
            matchedAppId = appId;
            return true;
        }
        return false;
    });
    try {
        const appURLObj = new URL(appURL);
        const appHostName = appURLObj.hostname;
        if (!matchedAppId && appHostName === 'localhost') {
            // matchedAppId = '_LOCALHOST';
        }
        if ((config.get('apps.trusted_origins') || []).includes(appURLObj.origin)) {
            return appURL;
        }
        const trustedHostNames = [...config.get('apps.trusted_hostnames') || []];
        if (selfHostName && trustedHostNames.includes('_')) trustedHostNames.push(selfHostName);
        if (trustedHostNames.includes(appURLObj.hostname)) return appURL;
    } catch (e) {
        if (isURLError(e)) {
            // invalid url
        } else {
            throw e;
        }
    }
    return matchedAppId;
}
