import { URL, URLSearchParams } from 'url';
import { request } from '@vedavaapi/web';

export class VedavaapiOAuthClient {
    constructor({ siteURL, clientId, clientSecret }) {
        this.siteURL = siteURL.replace(/\/$/, '');
        this.clientId = clientId;
        this.clientSecret = clientSecret;

        this.authURL = new URL('accounts/v1/oauth/authorize', `${this.siteURL}/`).href;
        this.tokenURL = new URL('accounts/v1/oauth/token', `${this.siteURL}/`).href;
    }

    redirectForAuthorization(res, redirectURL, state, scope = 'vedavaapi.root', response_type = 'code') {
        const fullAuthURL = new URL(this.authURL);
        const qParams = new URLSearchParams({
            response_type,
            client_id: this.clientId,
            redirect_uri: redirectURL,
            scope,
            state,
        });
        fullAuthURL.search = qParams.toString();
        res.redirect(fullAuthURL);
    }

    // eslint-disable-next-line class-methods-use-this
    extractAuthCode(req) {
        return req.query.code;
    }

    async exchangeCodeForAccessToken(authCode, registeredRedirectURI) {
        return request(this.tokenURL, {
            method: 'POST',
            body: new URLSearchParams({
                grant_type: 'authorization_code',
                client_id: this.clientId,
                client_secret: this.clientSecret,
                redirect_uri: registeredRedirectURI,
                code: authCode,
            }),
        });
    }

    async refreshAccessToken(refreshToken) {
        return request(this.tokenURL, {
            method: 'POST',
            body: new URLSearchParams({
                grant_type: 'refresh_token',
                client_id: this.clientId,
                client_secret: this.clientSecret,
                refresh_token: refreshToken,
            }),
        });
    }

    // eslint-disable-next-line class-methods-use-this
    extractAccessTokenFromResponse(atr) {
        return atr ? atr.access_token : null;
    }

    // eslint-disable-next-line class-methods-use-this
    extractRefreshTokenFromResponse(atr) {
        return atr ? atr.refresh_token : null;
    }
}
