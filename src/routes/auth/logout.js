import { SESSION_COOKIE_NAME } from '../../names';

export async function get(req, res) {
    // presently only clearing session incookies
    res.clearCookie(SESSION_COOKIE_NAME, { signed: true });
    res.json({
        status: 'SUCCESS',
        session: {},
    });
}
