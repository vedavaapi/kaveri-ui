import { URL } from 'url';

import { SESSION_COOKIE_NAME } from '../../names';
import { fullUrl } from './_helpers/urls';
import { oauthClient } from './_environ';

export async function get(req, res) {
    res.clearCookie(SESSION_COOKIE_NAME, { signed: true }); // we are blindly clearing it up.
    const callbackURL = new URL('oauth_callback', fullUrl(req)).href;
    oauthClient.redirectForAuthorization(res, callbackURL, '');
}
