import config from 'config';
// @ts-ignore
import site from '../../site.json';
import { VedavaapiOAuthClient } from './_helpers/oauth.js';

export const oauthClient = new VedavaapiOAuthClient({
    siteURL: site.url,
    clientId: config.get('oauth.client_id'),
    clientSecret: config.get('oauth.client_secret'),
});
