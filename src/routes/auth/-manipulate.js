/**
 * this is for developer conviniency, to manipulate session for convinient testing. trict no-cors.
 */


import { jsonBodyParser } from '../../body-parser-middlewares';
import { SESSION_COOKIE_NAME } from '../../names';

export async function post(req, res) {
    jsonBodyParser(req, res, () => {
        const { sessionUpdate } = req.body || {};
        const newSession = { ...req.ksession.session, ...sessionUpdate };
        res.cookie(SESSION_COOKIE_NAME, { ...req.ksession, session: newSession }, { signed: true });
        res.json({
            status: 'SUCCESS',
            session: newSession,
        });
    });
}
