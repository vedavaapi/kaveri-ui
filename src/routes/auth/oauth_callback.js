import delve from 'dlv';
import { accounts } from '@vedavaapi/client';
import { URLSearchParams, URL } from 'url';

// @ts-ignore
import site from '../../site.json';
import { fullUrl } from './_helpers/urls';
import { oauthClient } from './_environ';
import { SESSION_COOKIE_NAME } from '../../names';

export async function get(req, res) {
    const registeredRedirectURL = new URL('oauth_callback', fullUrl(req)).href;

    const authCode = oauthClient.extractAuthCode(req);
    let atr;
    let user;
    try {
        atr = (await oauthClient.exchangeCodeForAccessToken(authCode, registeredRedirectURL)).data;
        user = (await accounts.me.Me.get({
            vc: { base: site.url, accessToken: atr.access_token },
            params: {
                projection: {
                    _id: 1, name: 1, email: 1, 'externalAuthentications.google.picture': 1, 'externalAuthentications.google.name': 1,
                },
            },
        })).data;
    } catch (e) {
        res.send(`error in authorization ${e}`);
        // throw e;
        return;
    }

    user.name = user.name || delve(user, 'externalAuthentications.google.name', undefined);
    user.picture = user.picture || delve(user, 'externalAuthentications.google.picture', undefined);
    delete user.externalAuthentications;

    const session = {
        accessToken: atr.access_token,
        expiresIn: atr.expires_in,
        issuedAt: Date.now(),
        user,
    };
    res.cookie(SESSION_COOKIE_NAME, {
        session,
        refreshToken: oauthClient.extractRefreshTokenFromResponse(atr),
    }, { signed: true });
    res.status(200);

    const clientRedirectURL = new URL('client_callback', fullUrl(req)); // can remove this extra redirect, if all browsers support setting cookies on redirect.
    clientRedirectURL.search = new URLSearchParams({ event: 'AUTHORIZED' }).toString();

    res.send(`redirecting back to ui... <script>window.location = "${clientRedirectURL.href}";</script>`);
}
