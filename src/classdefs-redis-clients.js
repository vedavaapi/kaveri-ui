import { getRedisClient } from '@vedavaapi/classdefs/dist/iso-redis';
import { isBrowser } from '@vedavaapi/classdefs/dist/utils';

const connEctionOpts = { };
if (!isBrowser) {
    connEctionOpts.host = process.env.REDIS_SERVER_HOST || 'localhost';
    connEctionOpts.port = process.env.REDIS_SERVER_PORT || '6379';
}

export const metaStoreRedisClient = getRedisClient({ db: 1, ...connEctionOpts });

export const chStoreRedisClient = getRedisClient({ db: 2, ...connEctionOpts });

export const uiProfileStoreRedisClient = getRedisClient({ db: 3, ...connEctionOpts });
