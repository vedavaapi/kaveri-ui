import { readable, writable, derived, get } from 'svelte/store';
import { locale } from 'svelte-i18n';
import { LocaleContext } from '@vedavaapi/jsonld-helpers/dist/locale';
// @ts-ignore
// eslint-disable-next-line import/no-unresolved
import { stores } from '@sapper/app';
// @ts-ignore
import siteConfig from './site.json';

/**
 * readable store containing site details
 */
export const site = readable(siteConfig, undefined);

/**
 * store derived from sapper session, to import directly
 * we typically .init() it in _layout.svelte, on every page load, to use current session as primary truth to react against.
 */
export const session = (() => {
    const backStore = writable({}); // TODO set type
    const { subscribe, set } = backStore;

    const expireThreshold = 300000; // 5 minutes

    let sapSession;
    let expireTimeout;
    let expireThresholdTimeout;

    return {
        init() {
            sapSession = stores().session;

            sapSession.subscribe((sapSessionVal) => {
                clearTimeout(expireTimeout);
                clearTimeout(expireThresholdTimeout);

                const val = { ...(sapSessionVal || {}) };
                let expiresIn;
                if (!val.accessToken || !val.issuedAt || !val.expiresIn) {
                    expiresIn = -Infinity;
                } else {
                    expiresIn = (val.issuedAt + val.expiresIn * 1000) - Date.now();
                }
                val.authorized = expiresIn > 0;
                set(val);

                // @ts-ignore
                if (val.authorized && process.browser) {
                    expireTimeout = setTimeout(() => {
                        const curVal = get(backStore); // should be same as val though.
                        curVal.authorized = false;
                        set(curVal);
                    }, expiresIn);
                    expireThresholdTimeout = setTimeout(() => {
                        const curVal = get(backStore);
                        curVal.atExpiryThreshold = true;
                        set(curVal);
                    }, expiresIn - expireThreshold);
                }
            });
        },

        set(val) { sapSession.set(val); },

        subscribe,

        unsubscribe() {
            sapSession();
        },
    };
})();

/**
 * readonly reactive projection of sapper session.
 */
export const vc = derived([site, session], ([$site, $session]) => ({
    base: $site.url,
    accessToken: $session.accessToken,
    authorized: $session.authorized,
}));


export const lc = derived([locale], ([$locale]) => new LocaleContext({
    locale: $locale,
    fallbackLocales: ['en', '*'],
}));
