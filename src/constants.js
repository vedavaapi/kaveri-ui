export const ORD_ASC = 'ASC';

export const ORD_DESC = 'DESC';

export const ENUM_ORD = [ORD_ASC, ORD_DESC];


export const STATUS_PENDING = 'PENDING';

export const STATUS_PROGRESS = 'PROGRESS';

export const STATUS_SUCCESS = 'SUCCESS';

export const STATUS_FAILURE = 'FAILURE';

export const ENUM_STATUS = [
    STATUS_PENDING,
    STATUS_PROGRESS,
    STATUS_SUCCESS,
    STATUS_FAILURE,
];
