export function toSentenseCase(str) {
    return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
}


export function toTitleCase(str) {
    return str.replace(/\w\S*/g, toSentenseCase);
}
