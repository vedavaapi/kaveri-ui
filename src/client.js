// @ts-nocheck
// eslint-disable-next-line import/no-unresolved
import * as sapper from '@sapper/app';

import './i18n';


sapper.start({
    target: document.querySelector('#sapper'),
});
