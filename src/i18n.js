import { register, init } from 'svelte-i18n';

// @ts-ignore
register('en', () => import('../messages/en.json'))
// @ts-ignore
register('te', () => import('../messages/te.json'))
// @ts-ignore
register('sa', () => import('../messages/sa.json'))


init({
    fallbackLocale: 'en',
    initialLocale: {
        // navigator: true,
    },
});


export const LOCALE_LABELS = {
    en: 'en',
    te: 'తె',
    sa: 'सं',
    hi: 'हि',
};
