import sirv from 'sirv';
import delve from 'dlv';
import dset from 'dset';
import express from 'express';
// import cors from 'cors';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import accepts from 'accepts';
// import './set-config';
import config from 'config';
// @ts-ignore
// eslint-disable-next-line import/no-unresolved
import * as sapper from '@sapper/server';

import { SESSION_COOKIE_NAME, APPS_COOKIE_NAME } from './names';
import './i18n';

const { PORT, NODE_ENV, MOUNT_PATH = '/' } = process.env;
const dev = NODE_ENV === 'development';

const kSessionMiddleware = (req, res, next) => {
    // eslint-disable-next-line no-console
    // console.log(req.url);
    req.ksession = req.signedCookies[SESSION_COOKIE_NAME] || {};
    req.apps = req.signedCookies[APPS_COOKIE_NAME] || {};

    // We will store language from accept-language header to conf, if is not defined there.
    let lang = delve(req.ksession, 'session.conf.lang');
    if (!lang) {
        const acceptedLanguages = accepts(req).languages();
        if (acceptedLanguages && Array.isArray(acceptedLanguages) && acceptedLanguages.length > 0) {
            [lang] = acceptedLanguages[0].split('-');
            dset(req.ksession, 'session.conf.lang', lang);
        }
    }

    next();
};

const app = express();
app.set('trust proxy', true); // http://expressjs.com/en/guide/behind-proxies.html

app
    .use(
        MOUNT_PATH,
        compression({ threshold: 0 }),
        // cors(),
        sirv('static', { dev }),
        cookieParser(config.get('session.secret')),
        kSessionMiddleware,
        sapper.middleware({
            session: (req) => req.ksession.session || {},
        }),
    )
    .listen(PORT, (err) => {
        // eslint-disable-next-line no-console
        if (err) console.log('error', err);
    });

export default app;
