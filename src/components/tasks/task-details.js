import { request } from '@vedavaapi/web';

import { writable } from 'svelte/store';

export function pollStatus(statusURL) {
    const { set, subscribe } = writable(null);

    let isStopped = false;
    const pollStopper = () =>{
        isStopped = true;
    };

    const queryStatus = async () => {
        const resp = await request(statusURL);
        set(resp.data);
        if (['SUCCESS', 'FAILURE'].includes(resp.data.state)) {
            return true;
        }
        if (isStopped) {
            return false; // not polled till resolution;
        }
        return new Promise((resolve, reject) => { // async recursion with timeout
            setTimeout(async () => {
                try {
                    resolve(await queryStatus());
                } catch (e) {
                    reject(e);
                }
            }, 2000);
        });
    };

    return { taskDetails: { subscribe }, pollPromise: queryStatus(), pollStopper };
}
