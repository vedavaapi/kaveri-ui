
import '../../storybook-init';
import SimpleSearchCriteria from '../SimpleSearchCriteria.svelte';


export default {
    title: 'utils/SimpleSearchCriteria',
};


export const ideal = () => ({
    Component: SimpleSearchCriteria,
    props: {
        sortKeys: [{ id: 'email', label: 'Email' }, { id: 'name', label: 'Name' }, { id: '_id', label: 'Id' }],
        queryHint: 'Search by name',
    },
});
