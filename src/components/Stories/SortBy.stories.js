
import '../../storybook-init';
import SortBy from '../SortBy.svelte';


export default {
    title: 'utils/SortBy',
};


export const ideal = () => ({
    Component: SortBy,
    props: {
        keys: [{ id: 'email', label: 'Email' }, { id: 'name', label: 'Name' }, { id: '_id', label: 'Id' }],
    },
});
