
import '../../storybook-init';
import Modal from '../Modal.svelte';


export default {
    title: 'utils/Modal',
};


export const ideal = () => ({
    Component: Modal,
    props: {
        visible: true,
    },
});
