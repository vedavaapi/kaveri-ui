
import { FetchError } from '@vedavaapi/web';
import '../../storybook-init';
import FetchErrorReport from '../FetchErrorReport.svelte';


export default {
    title: 'utils/FetchErrorReport',
};


export const unauthorized = () => ({
    Component: FetchErrorReport,
    props: {
        error: new FetchError({
            url: 'a',
            networkError: false,
            response: {
                data: {
                    message: 'Not authorized',
                },
                status: 401,
                statusText: 'UNAUTHORIZED',
                ok: false,
                headers: {},
            },
        }),
        action: 'getting users',
    },
});


export const unauthorizedNoRetry = () => ({
    Component: FetchErrorReport,
    props: {
        error: new FetchError({
            url: 'a',
            networkError: false,
            response: {
                data: {
                    message: 'Not authorized',
                },
                status: 401,
                statusText: 'UNAUTHORIZED',
                ok: false,
                headers: {},
            },
        }),
        action: 'getting users',
        shouldAllowRetry: false,
    },
});


export const networkError = () => ({
    Component: FetchErrorReport,
    props: {
        error: new FetchError({
            url: 'a',
            networkError: true,
        }),
        action: 'Getting users',
    },
});
