import { FetchError } from '@vedavaapi/web';

import { STATUS_FAILURE, STATUS_SUCCESS, STATUS_PENDING , STATUS_PROGRESS} from '../../constants';
import '../../storybook-init';
import StatusScreen from '../StatusScreen.svelte';


export default {
    title: 'utils/StatusScreen',
};


export const failure = () => ({
    Component: StatusScreen,
    props: {
        error: new FetchError({
            url: 'a',
            networkError: false,
            response: {
                data: {
                    message: 'Not authorized',
                },
                status: 401,
                statusText: 'UNAUTHORIZED',
                ok: false,
                headers: {},
            },
        }),
        action: 'getting users',
        shouldAllowRetry: true,
        status: STATUS_FAILURE,
    },
});


export const success = () => ({
    Component: StatusScreen,
    props: {
        action: 'getting users',
        status: STATUS_SUCCESS,
    },
});


export const progress = () => ({
    Component: StatusScreen,
    props: {
        action: 'getting users',
        status: STATUS_PROGRESS,
    },
});
