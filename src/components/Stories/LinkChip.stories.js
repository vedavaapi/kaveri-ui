
import '../../storybook-init';
import LinkChip from '../LinkChip.svelte';


export default {
    title: 'utils/LinkChip',
};


export const ideal = () => ({
    Component: LinkChip,
    props: {
        href: 'a',
        text: 'Open in Mirador',
        iconClass: 'cog',
    },
});
