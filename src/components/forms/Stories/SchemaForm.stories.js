import '../../../storybook-init';
import SchemaForm from '../SchemaForm.svelte';

export default {
    title: 'forms/SchemaForm',
};

export const ideal = () => ({
    Component: SchemaForm,
    props: {
        schema: {
            title: 'Book upload',
            fields: [
                { name: 'name', type: 'string', label: 'Name', required: true, descr: 'name of the book' },
                { name: 'description', type: 'text', label: 'Description' },
                { name: 'source', type: '_id', label: 'parent', allowedClasses: ['Library'] },
                { name: 'cover', type: 'image', label: 'Cover' },
                { name: 'index', type: 'number', label: 'Index' },
            ],
        },
        initialValues: {
            source: '5e52975d3bc959000ae2ffb1',
        },
    },
});
