
import '../../../storybook-init';
import ImageFileField from '../FileField.svelte';


export default {
    title: 'forms/ImageFileField',
};


export const ideal = () => ({
    Component: ImageFileField,
    props: {
        currentRepr: {
            url: 'dakshinamurthi.png',
        },
    },
});
