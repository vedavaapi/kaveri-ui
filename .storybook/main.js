
module.exports = {
  stories: ['../src/**/*.stories.js'],
  addons: ['@storybook/addon-actions', '@storybook/addon-links'],
  webpackFinal: (config) => {
      config.externals = {
          'node-fetch': 'node-fetch',
          'fetch-cookie/node-fetch': 'fetch-cookie/node-fetch',
          'form-data': 'form-data',
          'url': 'url',
          "redis": "redis",
      };
      return config;
  },
};
